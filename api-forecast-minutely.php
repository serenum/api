<?php

	$arr_wea_minutely = [];
	if(isset($data_weather->minutely)) {
		foreach($data_weather->minutely AS $minutely) {
			$weather_dt_data = (!isset($minutely->dt) ? null : $minutely->dt);
			$weather_m_precip_mm = (!isset($minutely->precipitation) ? null : $minutely->precipitation);
			$weather_m_precip_cm = ($weather_m_precip_mm == null ? null : convert($weather_m_precip_mm, 'cm'));
			$weather_m_precip_in = ($weather_m_precip_mm == null ? null : convert($weather_m_precip_mm, 'in'));

			$correct_hour = new DateTime(date('Y-m-d, H:i', $weather_dt_data));
			$correct_hour->setTimeZone(new DateTimeZone($timezone));
			$weather_dt_data = strtotime($correct_hour->format('Y-m-d H:i:s'));

			$arr_wea_minutely[] = [
				'timestamp' => $weather_dt_data,
				'date' => date_($weather_dt_data, 'date'),
				'time' => arr_time($weather_dt_data),
				'precipitation' => [
					'mm' => (float)format_number($weather_m_precip_mm, 3, '.'),
					'cm' => (float)format_number($weather_m_precip_cm, 3, '.'),
					'in' => (float)format_number($weather_m_precip_in, 3, '.')
				]
			];
		}
	}

?>