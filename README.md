## Serenum API

### !!! New version is coming soon! API changing from OpenWeatherMap to [MET](https://api.met.no/) due to privacy reasons. New API structure and features.

### What is Serenum API?
Serenum API is a weather API that uses OpenWeatherMap API and aims to be as detailed and simple as it can possible be. You can choose to call the data from api.serenum.org or download the files and use it on your own server. It requires your own API key from OpenWeatherMap. Read more: https://api.serenum.org/wiki#download

### Features
- Current timezone, date, and time for the given place
- Detailed overview for the given place
  - Direct link to OpenStreetMap to the coordinates
- Current weather
  - Current weather's id, icon, and description
  - Probability of rain and/or snow
  - Temperature with feels like temperature + dew point
  - Wind speed and direction
  - Cloud cover
  - Humidity
  - Pressure
  - UV index
  - Grams of H2O per kg of air
  - Air pollution
- Global weather map from yr.no (clouds only)
- Weather forecast
  - Minutely for 1 hour (probability only)
  - Hourly for 24 hours
  - Daily for 7 days
  - Astronomical data about the hourly and daily forecast
- Astronomy data
  - Sun and Moon hours
  - Sun and Moon positions
  - Moon's distance to Earth
  - Moon's distance to the Sun
  - The current phase of the Moon
  - Forecast of Moon phases
    - Countdowns to the Moon phases
  - Moon's angle, age, and fraction
  - If the Sun are up or not
  - If the Moon are always up or always down
  - Daylight and night time
  - Support for Stellarium API for more accurate data about the Sun and the Moon. This requires the Stellarium software to be installed on the machine and the RemoteControl plugin to be enabled
- Compare 2 places with `diff.php`
  - See the current weather for both of the places. Is it for an example warmer on the first place compared to the second place?
  - See the distance (bird's eye view) and the difference between both places.

### Plans
- Add thunderstorm data for the current weather and maybe also for the forecast
- Show if the Sun are always up or always down

----

### Bugs
- `class-suncalc.php` shows wrong hours for the Sun. Please see https://kb.airikr.me/?controller=TaskViewController&action=readonly&task_id=115&token=ec537fbb56f25a10001af6028bb97e8fb29cc51ba74ca9814abcf26cd9f8
- The regex for validating the coordinates do validates `592697,14.10089` which is wrong

### Contact us
If you want to see all of our plans and the issues we have encountered, please go to [our project management page](https://kb.airikr.me/?controller=BoardViewController&action=readonly&token=ec537fbb56f25a10001af6028bb97e8fb29cc51ba74ca9814abcf26cd9f8). For any issues, [create a new issue](https://codeberg.org/serenum/api/issues/new) or write to us on [Telegram](https://t.me/serenumfoss).

----

### How to get started
1. Clone the repo or download the files.
2. Change `[your-api-key]` with your own API key from [OpenWeatherMap](https://openweathermap.com) or append `&key=[your-key-from-owm]` to the address.
3. Visit http://localhost/api in your browser and follow the instructions.

### How to get started with the difference page
1. Clone the repo or download the files.
2. Change `[your-api-key]` with your own API key from [OpenWeatherMap](https://openweathermap.com) or append `&key=[your-key-from-owm]` to the address.
3. Visit http://localhost/api/diff.php in your browser and follow the instructions.