<?php

	/*
		IMPORTANT!
		Ambee Pollen API have been removed from Serenum API due to very slow response time.
		We are currently looking for another solution. If you know any solution, contact hi@serenum.org or create an new issue. Thanks.
	*/


	if($enable_pollen == true AND !in_array('pollen', $arr_excluded)) {
		$arr_pollen = [
			'message' => ($pollen == null ? 'Couldn\'t get pollen data.' : null),
			'updated' => [
				'timestamp' => ($pollen == null ? null : strtotime($pollen_updatedat)),
				'date' => ($pollen == null ? null : date_(strtotime($pollen_updatedat), 'date')),
				'time' => ($pollen == null ? null : arr_time(strtotime($pollen_updatedat)))
			],
			'grass' => [
				'risk' => $pollen_risk_grass,
				'count' => $pollen_species_grass_poaceae,
				'species' => [
					'grass_poaceae' => $pollen_species_grass_poaceae
				]
			],
			'tree' => [
				'risk' => $pollen_risk_tree,
				'count' => ($pollen == null ? null : ($pollen_species_tree_alder + $pollen_species_tree_birch + $pollen_species_tree_cypress + $pollen_species_tree_elm + $pollen_species_tree_hazel + $pollen_species_tree_oak + $pollen_species_tree_pine + $pollen_species_tree_plane + $pollen_species_tree_poplar_cottonwood)),
				'species' => [
					'alder' => $pollen_species_tree_alder,
					'birch' => $pollen_species_tree_birch,
					'cypress' => $pollen_species_tree_cypress,
					'elm' => $pollen_species_tree_elm,
					'hazel' => $pollen_species_tree_hazel,
					'oak' => $pollen_species_tree_oak,
					'pine' => $pollen_species_tree_pine,
					'plane' => $pollen_species_tree_plane,
					'poplar_cottonwood' => $pollen_species_tree_poplar_cottonwood
				]
			],
			'weed' => [
				'risk' => $pollen_risk_weed,
				'count' => ($pollen == null ? null : ($pollen_species_weed_chenapod + $pollen_species_weed_mugwort + $pollen_species_weed_nettle + $pollen_species_weed_ragweed)),
				'species' => [
					'chenapod' => $pollen_species_weed_chenapod,
					'mugwort' => $pollen_species_weed_mugwort,
					'nettle' => $pollen_species_weed_nettle,
					'ragweed' => $pollen_species_weed_ragweed
				]
			]
		];
	}

?>