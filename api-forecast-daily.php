<?php

	$arr_wea_daily = [];
	foreach($data_weather->daily AS $daily) {
		$weather_dt_data = (!isset($daily->dt) ? null : $daily->dt);
		$weather_d_id = (!isset($daily->weather[0]->id) ? null : $daily->weather[0]->id);
		$weather_d_ic = (!isset($daily->weather[0]->icon) ? null : $daily->weather[0]->icon);
		$weather_d_wm = (!isset($daily->weather[0]->main) ? null : $daily->weather[0]->main);
		$weather_d_wd = (!isset($daily->weather[0]->description) ? null : $daily->weather[0]->description);
		$weather_d_p = (!isset($daily->pop) ? null : convert($daily->pop, 'percent'));
		$weather_d_r_mm = (!isset($daily->rain) ? null : $daily->rain);
		$weather_d_r_cm = ($weather_d_r_mm == null ? null : convert($weather_d_r_mm, 'cm'));
		$weather_d_r_in = ($weather_d_r_mm == null ? null : convert($weather_d_r_mm, 'in'));
		$weather_d_s_mm = (!isset($daily->snow) ? null : $daily->snow);
		$weather_d_s_cm = ($weather_d_s_mm == null ? null : convert($weather_d_s_mm, 'cm'));
		$weather_d_s_in = ($weather_d_s_mm == null ? null : convert($weather_d_s_mm, 'in'));

		$weather_d_tmi_c = (!isset($daily->temp->min) ? null : $daily->temp->min);
		$weather_d_tmi_f = ($weather_d_tmi_c == null ? null : convert($weather_d_tmi_c, 'f'));
		$weather_d_tmi_k = ($weather_d_tmi_c == null ? null : convert($weather_d_tmi_c, 'k'));
		$weather_d_tma_c = (!isset($daily->temp->max) ? null : $daily->temp->max);
		$weather_d_tma_f = ($weather_d_tma_c == null ? null : convert($weather_d_tma_c, 'f'));
		$weather_d_tma_k = ($weather_d_tma_c == null ? null : convert($weather_d_tma_c, 'k'));
		$weather_d_tm_c = (!isset($daily->temp->morn) ? null : $daily->temp->morn);
		$weather_d_tm_f = ($weather_d_tm_c == null ? null : convert($weather_d_tm_c, 'f'));
		$weather_d_tm_k = ($weather_d_tm_c == null ? null : convert($weather_d_tm_c, 'k'));
		$weather_d_td_c = (!isset($daily->temp->day) ? null : $daily->temp->day);
		$weather_d_td_f = ($weather_d_td_c == null ? null : convert($weather_d_td_c, 'f'));
		$weather_d_td_k = ($weather_d_td_c == null ? null : convert($weather_d_td_c, 'k'));
		$weather_d_te_c = (!isset($daily->temp->eve) ? null : $daily->temp->eve);
		$weather_d_te_f = ($weather_d_te_c == null ? null : convert($weather_d_te_c, 'f'));
		$weather_d_te_k = ($weather_d_te_c == null ? null : convert($weather_d_te_c, 'k'));
		$weather_d_tn_c = (!isset($daily->temp->night) ? null : $daily->temp->night);
		$weather_d_tn_f = ($weather_d_tn_c == null ? null : convert($weather_d_tn_c, 'f'));
		$weather_d_tn_k = ($weather_d_tn_c == null ? null : convert($weather_d_tn_c, 'k'));

		$weather_d_tflm_c = (!isset($daily->feels_like->morn) ? null : $daily->feels_like->morn);
		$weather_d_tflm_f = ($weather_d_tflm_c == null ? null : convert($weather_d_tflm_c, 'f'));
		$weather_d_tflm_k = ($weather_d_tflm_c == null ? null : convert($weather_d_tflm_c, 'k'));
		$weather_d_tfld_c = (!isset($daily->feels_like->day) ? null : $daily->feels_like->day);
		$weather_d_tfld_f = ($weather_d_tfld_c == null ? null : convert($weather_d_tfld_c, 'f'));
		$weather_d_tfld_k = ($weather_d_tfld_c == null ? null : convert($weather_d_tfld_c, 'k'));
		$weather_d_tfle_c = (!isset($daily->feels_like->eve) ? null : $daily->feels_like->eve);
		$weather_d_tfle_f = ($weather_d_tfle_c == null ? null : convert($weather_d_tfle_c, 'f'));
		$weather_d_tfle_k = ($weather_d_tfle_c == null ? null : convert($weather_d_tfle_c, 'k'));
		$weather_d_tfln_c = (!isset($daily->feels_like->night) ? null : $daily->feels_like->night);
		$weather_d_tfln_f = ($weather_d_tfln_c == null ? null : convert($weather_d_tfln_c, 'f'));
		$weather_d_tfln_k = ($weather_d_tfln_c == null ? null : convert($weather_d_tfln_c, 'k'));

		$weather_d_tdp_c = (!isset($daily->dew_point) ? null : $daily->dew_point);
		$weather_d_tdp_f = ($weather_d_tdp_c == null ? null : convert($weather_d_tdp_c, 'f'));
		$weather_d_tdp_k = ($weather_d_tdp_c == null ? null : convert($weather_d_tdp_c, 'k'));

		$weather_d_ws_ms = (!isset($daily->wind_speed) ? null : $daily->wind_speed);
		$weather_d_ws_g_ms = (!isset($daily->wind_gust) ? null : $daily->wind_gust);
		$weather_d_ws_kmh = ($weather_d_ws_ms == null ? null : convert($weather_d_ws_ms, 'kmh'));
		$weather_d_ws_g_kmh = ($weather_d_ws_g_ms == null ? null : convert($weather_d_ws_g_ms, 'kmh'));
		$weather_d_ws_mph = ($weather_d_ws_ms == null ? null : convert($weather_d_ws_ms, 'mph'));
		$weather_d_ws_g_mph = ($weather_d_ws_g_ms == null ? null : convert($weather_d_ws_g_ms, 'mph'));
		$weather_d_ws_kt = ($weather_d_ws_ms == null ? null : convert($weather_d_ws_ms, 'knot'));
		$weather_d_ws_g_kt = ($weather_d_ws_g_ms == null ? null : convert($weather_d_ws_g_ms, 'knot'));
		$weather_d_wd_deg = (!isset($daily->wind_deg) ? null : $daily->wind_deg);
		$weather_d_c = (!isset($daily->clouds) ? null : $daily->clouds);
		$weather_d_uvi = (!isset($daily->uvi) ? null : $daily->uvi);
		$weather_d_pr = (!isset($daily->pressure) ? null : $daily->pressure);
		$weather_d_pr_pa = (!isset($daily->pressure) ? null : convert($weather_d_pr, 'pa'));
		$weather_d_pr_bar = (!isset($daily->pressure) ? null : convert($weather_d_pr, 'bar'));
		$weather_d_pr_mmhg = (!isset($daily->pressure) ? null : convert($weather_d_pr, 'mmhg'));
		$weather_d_pr_inhg = (!isset($daily->pressure) ? null : convert($weather_d_pr, 'inhg'));
		$weather_d_pr_psi = (!isset($daily->pressure) ? null : convert($weather_d_pr, 'psi'));
		$weather_d_pr_atm = (!isset($daily->pressure) ? null : convert($weather_d_pr, 'atm'));
		$weather_d_h = (!isset($daily->humidity) ? null : $daily->humidity);
		$weather_d_h20perkgofair = ($weather_d_h == null ? null : calc_h2o_perkg_air($weather_d_h, $weather_d_td_c));

		$correct_hour = new DateTime(date('Y-m-d, H:i', $weather_dt_data));
		$correct_hour->setTimeZone(new DateTimeZone($timezone));
		$weather_dt_data = strtotime($correct_hour->format('Y-m-d H:i:s'));

		$astro = new AurorasLive\SunCalc($correct_hour, $latitude, $longitude);
		$astro_sun_times = $astro->getSunTimes();
		$astro_sun_position = $astro->getSunPosition();
		$astro_moon_position = $astro->getMoonPosition($correct_hour);
		$astro_moon_illumination = $astro->getMoonIllumination($correct_hour);
		$astro_moon_times = $astro->getMoonTimes($correct_hour);

		$astro_sun_altitude = ($astro_sun_position->altitude * 180 / M_PI);
		$astro_sun_azimuth = ($astro_sun_position->azimuth * 180 / M_PI);
		$astro_sun_isup = ($astro_sun_altitude < 0 ? false : true);
		$astro_sun_rise = ($astro_sun_times['sunrise'] == null ? null : strtotime($astro_sun_times['sunrise']->format('Y-m-d H:i:s')));
		$astro_sun_set = ($astro_sun_times['sunset'] == null ? null : strtotime($astro_sun_times['sunset']->format('Y-m-d H:i:s')));

		$astro_moon_altitude = ($astro_moon_position->altitude * 180 / M_PI);
		$astro_moon_azimuth = ($astro_moon_position->azimuth * 180 / M_PI);
		$astro_moon_rise = (!isset($astro_moon_times['moonrise']) ? null : strtotime($astro_moon_times['moonrise']->format('Y-m-d H:i:s')));
		$astro_moon_set = (!isset($astro_moon_times['moonset']) ? null : strtotime($astro_moon_times['moonset']->format('Y-m-d H:i:s')));
		$astro_moon_age = $astro_moon->get('age');
		$astro_moon_phase = $astro_moon_illumination['phase'];
		$astro_moon_phase_name = $astro_moon->phase_name();
		$astro_moon_fraction = $astro_moon_illumination['fraction'];
		$astro_moon_angle = $astro_moon_illumination['angle'];
		$astro_moon_distance_earth = $astro_moon->get('distance');
		$astro_moon_distance_sun = $astro_moon->get('sundistance');
		$astro_moon_diameter_moon = $astro_moon->get('diameter');
		$astro_moon_diameter_sun = $astro_moon->get('sundiameter');

		$daylight_rise = new DateTime(date_($astro_sun_rise, 'datetime'), new DateTimeZone($timezone));
		$daylight_set = new DateTime(date_($astro_sun_set, 'datetime'), new DateTimeZone($timezone));
		$daylight = $daylight_set->diff($daylight_rise);

		$night_starts = new DateTime(date_($astro_sun_night, 'datetime'), new DateTimeZone($timezone));
		$night_ends = new DateTime(date_($astro_sun_rise, 'datetime'), new DateTimeZone($timezone));
		$night = $night_starts->diff($night_ends);



		$arr_wea_daily[] = [
			'timestamp' => $weather_dt_data,
			'date' => [
				'date' => date_($weather_dt_data, 'date'),
				'details' => [
					'year' => (int)date('Y', $weather_dt_data),
					'month' => [
						'number' => (int)date('n', $weather_dt_data),
						'name' => [
							'full' => date('F', $weather_dt_data),
							'short' => date('M', $weather_dt_data)
						]
					],
					'week' => (int)date('W', $weather_dt_data),
					'day' => [
						'number' => (int)date('j', $weather_dt_data),
						'suffix' => date('S', $weather_dt_data),
						'day_of_year' => (int)date('z', $weather_dt_data),
						'weekday' => [
							'full' => date('l', $weather_dt_data),
							'short' => date('D', $weather_dt_data)
						]
					]
				]
			],
			'time' => arr_time($weather_dt_data),
			'weather' => [
				'id' => $weather_d_id,
				'id_icon' => $weather_d_ic,
				'icon' => weathericon($weather_d_ic . $weather_d_id),
				'main' => $weather_d_wm,
				'description' => $weather_d_wd,
				'prob_precip' => (float)format_number($weather_d_p, 0),
				'rain' => [
					'mm' => (float)format_number($weather_d_r_mm, 2, '.'),
					'cm' => (float)format_number($weather_d_r_cm, 2, '.'),
					'in' => (float)format_number($weather_d_r_in, 2, '.')
				],
				'snow' => [
					'mm' => (float)format_number($weather_d_s_mm, 2, '.'),
					'cm' => (float)format_number($weather_d_s_cm, 2, '.'),
					'in' => (float)format_number($weather_d_s_in, 2, '.')
				]
			],
			'temperatures' => [
				'min' => [
					'celcius' => (float)format_number($weather_d_tmi_c, 2, '.'),
					'fahrenheit' => (float)format_number($weather_d_tmi_f, 2, '.'),
					'kelvin' => (float)format_number($weather_d_tmi_k, 2, '.')
				],
				'max' => [
					'celcius' => (float)format_number($weather_d_tma_c, 2, '.'),
					'fahrenheit' => (float)format_number($weather_d_tma_f, 2, '.'),
					'kelvin' => (float)format_number($weather_d_tma_k, 2, '.')
				],
				'morning' => [
					'hour' => [
						'twentyfour' => 6,
						'twelve' => 6,
						'ampm' => 'AM'
					],
					'celcius' => (float)format_number($weather_d_tm_c, 2, '.'),
					'fahrenheit' => (float)format_number($weather_d_tm_f, 2, '.'),
					'kelvin' => (float)format_number($weather_d_tm_k, 2, '.')
				],
				'day' => [
					'hour' => [
						'twentyfour' => 12,
						'twelve' => 12,
						'ampm' => 'PM'
					],
					'celcius' => (float)format_number($weather_d_td_c, 2, '.'),
					'fahrenheit' => (float)format_number($weather_d_td_f, 2, '.'),
					'kelvin' => (float)format_number($weather_d_td_k, 2, '.')
				],
				'evening' => [
					'hour' => [
						'twentyfour' => 18,
						'twelve' => 6,
						'ampm' => 'PM'
					],
					'celcius' => (float)format_number($weather_d_te_c, 2, '.'),
					'fahrenheit' => (float)format_number($weather_d_te_f, 2, '.'),
					'kelvin' => (float)format_number($weather_d_te_k, 2, '.')
				],
				'night' => [
					'hour' => [
						'twentyfour' => 0,
						'twelve' => 12,
						'ampm' => 'AM'
					],
					'celcius' => (float)format_number($weather_d_tn_c, 2, '.'),
					'fahrenheit' => (float)format_number($weather_d_tn_f, 2, '.'),
					'kelvin' => (float)format_number($weather_d_tn_k, 2, '.')
				]
			],
			'temperature_feels_like' => [
				'morning' => [
					'hour' => [
						'twentyfour' => 6,
						'twelve' => 6,
						'ampm' => 'AM'
					],
					'celcius' => (float)format_number($weather_d_tflm_c, 2, '.'),
					'fahrenheit' => (float)format_number($weather_d_tflm_f, 2, '.'),
					'kelvin' => (float)format_number($weather_d_tflm_k, 2, '.')
				],
				'day' => [
					'hour' => [
						'twentyfour' => 12,
						'twelve' => 12,
						'ampm' => 'PM'
					],
					'celcius' => (float)format_number($weather_d_tfld_c, 2, '.'),
					'fahrenheit' => (float)format_number($weather_d_tfld_f, 2, '.'),
					'kelvin' => (float)format_number($weather_d_tfld_k, 2, '.')
				],
				'evening' => [
					'hour' => [
						'twentyfour' => 18,
						'twelve' => 6,
						'ampm' => 'PM'
					],
					'celcius' => (float)format_number($weather_d_tfle_c, 2, '.'),
					'fahrenheit' => (float)format_number($weather_d_tfle_f, 2, '.'),
					'kelvin' => (float)format_number($weather_d_tfle_k, 2, '.')
				],
				'night' => [
					'hour' => [
						'twentyfour' => 0,
						'twelve' => 12,
						'ampm' => 'AM'
					],
					'celcius' => (float)format_number($weather_d_tfln_c, 2, '.'),
					'fahrenheit' => (float)format_number($weather_d_tfln_f, 2, '.'),
					'kelvin' => (float)format_number($weather_d_tfln_k, 2, '.')
				]
			],
			'temperature_dew_point' => [
				'celcius' => (float)format_number($weather_d_tdp_c, 2, '.'),
				'fahrenheit' => (float)format_number($weather_d_tdp_f, 2, '.'),
				'kelvin' => (float)format_number($weather_d_tdp_k, 2, '.')
			],
			'wind' => [
				'descriptions' => [
					'land' => wind_descriptions((float)format_number($weather_d_ws_ms, 1, '.'), 'land'),
					'sea' => wind_descriptions((float)format_number($weather_d_ws_ms, 1, '.'), 'sea')
				],
				'speed' => [
					'ms' => [
						'wind' => (float)format_number($weather_d_ws_ms, 1, '.'),
						'gust' => ($weather_d_ws_g_ms == null ? null : (float)format_number($weather_d_ws_g_ms, 1, '.'))
					],
					'kmh' => [
						'wind' => (float)format_number($weather_d_ws_kmh, 1, '.'),
						'gust' => ($weather_d_ws_g_kmh == null ? null : (float)format_number($weather_d_ws_g_kmh, 1, '.'))
					],
					'mph' => [
						'wind' => (float)format_number($weather_d_ws_mph, 1, '.'),
						'gust' => ($weather_d_ws_g_mph == null ? null : (float)format_number($weather_d_ws_g_mph, 1, '.'))
					],
					'knot' => [
						'wind' => (float)format_number($weather_d_ws_kt, 1, '.'),
						'gust' => ($weather_d_ws_g_kt == null ? null : (float)format_number($weather_d_ws_g_kt, 1, '.'))
					]
				],
				'direction' => [
					'degrees' => (int)$weather_d_wd_deg,
					'compass' => degrees_to_compass((int)$weather_d_wd_deg)
				],
				'beaufort' => beaufort($weather_d_ws_kt)
			],
			'clouds' => (int)$weather_d_c,
			'humidity' => (int)$weather_d_h,
			'h2o_per_kg_of_air' => [
				'value' => format_number((float)$weather_d_h20perkgofair, 2, '.', true),
				'type' => 'g'
			],
			'pressure' => [
				'hpa' => (int)$weather_d_pr,
				'pa' => (int)$weather_d_pr_pa,
				'bar' => (float)format_number($weather_d_pr_bar, 4, '.'),
				'mmhg' => (float)format_number($weather_d_pr_mmhg, 4, '.'),
				'inhg' => (float)format_number($weather_d_pr_inhg, 4, '.'),
				'psi' => (float)format_number($weather_d_pr_psi, 4, '.'),
				'atm' => (float)format_number($weather_d_pr_atm, 4, '.')
			],
			'uv_index' => [
				"grade" => (int)$weather_d_uvi,
				"risk" => uvindex_grade((int)$weather_d_uvi, 'risk'),
				"colour" => uvindex_grade((int)$weather_d_uvi, 'colour')
			],
			'astronomy' => [
				'sun' => [
					'durations' => [
						'daylight' => [
							'hours' => $daylight->h,
							'minutes' => $daylight->i
						],
						'night' => [
							'hours' => $night->h,
							'minutes' => $night->i
						]
					],
					'rise' => [
						'time' => arr_time($astro_sun_rise)
					],
					'set' => [
						'time' => arr_time($astro_sun_set)
					]
				],
				'moon' => [
					'phase' => $astro_moon_phase_name,
					'rise' => arr_time($astro_moon_rise),
					'set' => arr_time($astro_moon_set)
				]
			]
		];
	}

?>