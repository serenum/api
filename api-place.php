<?php

	if(!in_array('place', $arr_excluded)) {
		$arr_place = [
			'historic' => $geo_historic,
			'highway' => $geo_highway,
			'road' => $geo_road,
			'neighbourhood' => $geo_neighbourhood,
			'suburb' => $geo_suburb,
			'hamlet' => $geo_hamlet,
			'village' => $geo_village,
			'town' => $geo_town,
			'city' => $geo_city,
			'state' => $geo_state,
			'state_district' => $geo_state_district,
			'municipality' => $geo_municipality,
			'county' => $geo_county,
			'post_code' => $geo_postcode,
			'country' => $geo_country,
			'country_code' => $geo_country_code,
			'full' => $geo_full,
			'link' => 'https://openstreetmap.org/?mlat='.$latitude.'&mlon='.$longitude.'&zoom=15'
		];
	}

?>