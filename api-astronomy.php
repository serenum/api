<?php

	if(!in_array('astronomy', $arr_excluded)) {
		$daylight_rise = new DateTime(date_($astro_sun_rise, 'datetime'), new DateTimeZone($timezone));
		$daylight_set = new DateTime(date_($astro_sun_set, 'datetime'), new DateTimeZone($timezone));
		$daylight = $daylight_set->diff($daylight_rise);

		$night_starts = new DateTime(date_($astro_sun_night, 'datetime'), new DateTimeZone($timezone));
		$night_ends = new DateTime(date_($astro_sun_rise, 'datetime'), new DateTimeZone($timezone));
		$night = $night_starts->diff($night_ends);

		$arr_astro_sun = [
			'altitude' => (float)format_number($astro_sun_altitude, 15, '.'),
			'azimuth' => (float)format_number($astro_sun_azimuth, 15, '.'),
			'is_above_horizon' => $astro_sun_isup,
			'always_up' => (($daylight->h == 0 AND $daylight->i == 0 OR $astro_sun_rise == null) ? true : false),
			'always_down' => (($night->h == 0 AND $night->i == 0 OR $astro_sun_night == null) ? true : false),
			'durations' => [
				'daylight' => [
					'hours' => $daylight->h,
					'minutes' => $daylight->i
				],
				'night' => [
					'hours' => $night->h,
					'minutes' => $night->i
				]
			],
			'rise' => [
				'time' => arr_time($astro_sun_rise)
			],
			'rise_end' => [
				'time' => arr_time($astro_sun_riseend)
			],
			'goldenhour_end' => [
				'time' => arr_time($astro_sun_goldenhourend)
			],
			'solarnoon' => [
				'date' => date_($astro_sun_solarnoon, 'date'),
				'time' => arr_time($astro_sun_solarnoon)
			],
			'goldenhour' => [
				'time' => arr_time($astro_sun_goldenhour)
			],
			'set_start' => [
				'time' => arr_time($astro_sun_setstart)
			],
			'set' => [
				'time' => arr_time($astro_sun_set)
			],
			'dusk' => [
				'time' => arr_time($astro_sun_dusk)
			],
			'nautical_dusk' => [
				'time' => arr_time($astro_sun_nauticaldusk)
			],
			'night' => [
				'time' => arr_time($astro_sun_night)
			],
			'nadir' => [
				'time' => arr_time($astro_sun_nadir)
			],
			'night_end' => [
				'time' => arr_time($astro_sun_nightend)
			],
			'nautical_dawn' => [
				'time' => arr_time($astro_sun_nauticaldawn)
			],
			'dawn' => [
				'time' => arr_time($astro_sun_dawn)
			]
		];

		$arr_astro_moon = [
			'phase' => [
				'phase' => (float)format_number($astro_moon_phase, 15, '.'),
				'name' => $astro_moon_phase_name
			],
			'altitude' => (float)format_number($astro_moon_altitude, 15, '.'),
			'azimuth' => (float)format_number($astro_moon_azimuth, 15, '.'),
			'fraction' => (float)format_number($astro_moon_fraction, 1, '.'),
			'angle' => (float)format_number($astro_moon_angle, 1, '.'),
			'age' => (float)format_number($astro_moon_age, 1, '.'),
			'distances' => [
				'earth' => (float)str_replace(' ', '', format_number($astro_moon_distance_earth, 2, '.')),
				'sun' => (float)str_replace(' ', '', format_number($astro_moon_distance_sun, 2, '.'))
			],
			'diameters' => [
				'moon' => (float)str_replace(' ', '', format_number($astro_moon_diameter_moon, 2, '.')),
				'sun' => (float)str_replace(' ', '', format_number($astro_moon_diameter_sun, 2, '.'))
			],
			'rise' => arr_time($astro_moon_rise),
			'set' => arr_time($astro_moon_set),
			'always_up' => ($astro_moon_alwaysup != null ? true : false),
			'always_down' => ($astro_moon_alwaysdown != null ? true : false),
			'schedules' => [
				'first_quarter' => [
					'timestamp' => $astro_moon_firstquarter,
					'date' => date_($astro_moon_firstquarter, 'date'),
					'time' => arr_time($astro_moon_firstquarter),
					'countdown' => [
						'days' => timeleft($astro_moon_firstquarter, true),
						'hours' => timeleft($astro_moon_firstquarter, false, true),
						'minutes' => timeleft($astro_moon_firstquarter, false, false, true)
					]
				],
				'next_first_quarter' => [
					'timestamp' => $astro_moon_nextfirstquarter,
					'date' => date_($astro_moon_nextfirstquarter, 'date'),
					'time' => arr_time($astro_moon_nextfirstquarter),
					'countdown' => [
						'days' => timeleft($astro_moon_nextfirstquarter, true),
						'hours' => timeleft($astro_moon_nextfirstquarter, false, true),
						'minutes' => timeleft($astro_moon_nextfirstquarter, false, false, true)
					]
				],
				'full' => [
					'timestamp' => $astro_moon_full,
					'date' => date_($astro_moon_full, 'date'),
					'time' => arr_time($astro_moon_full),
					'countdown' => [
						'days' => timeleft($astro_moon_full, true),
						'hours' => timeleft($astro_moon_full, false, true),
						'minutes' => timeleft($astro_moon_full, false, false, true)
					]
				],
				'next_full' => [
					'timestamp' => $astro_moon_nextfull,
					'date' => date_($astro_moon_nextfull, 'date'),
					'time' => arr_time($astro_moon_nextfull),
					'countdown' => [
						'days' => timeleft($astro_moon_nextfull, true),
						'hours' => timeleft($astro_moon_nextfull, false, true),
						'minutes' => timeleft($astro_moon_nextfull, false, false, true)
					]
				],
				'last_quarter' => [
					'timestamp' => $astro_moon_lastquarter,
					'date' => date_($astro_moon_lastquarter, 'date'),
					'time' => arr_time($astro_moon_lastquarter),
					'countdown' => [
						'days' => timeleft($astro_moon_lastquarter, true),
						'hours' => timeleft($astro_moon_lastquarter, false, true),
						'minutes' => timeleft($astro_moon_lastquarter, false, false, true)
					]
				],
				'next_last_quarter' => [
					'timestamp' => $astro_moon_nextlastquarter,
					'date' => date_($astro_moon_nextlastquarter, 'date'),
					'time' => arr_time($astro_moon_nextlastquarter),
					'countdown' => [
						'days' => timeleft($astro_moon_nextlastquarter, true),
						'hours' => timeleft($astro_moon_nextlastquarter, false, true),
						'minutes' => timeleft($astro_moon_nextlastquarter, false, false, true)
					]
				],
				'new' => [
					'timestamp' => $astro_moon_new,
					'date' => date_($astro_moon_new, 'date'),
					'time' => arr_time($astro_moon_new),
					'countdown' => [
						'days' => timeleft($astro_moon_new, true),
						'hours' => timeleft($astro_moon_new, false, true),
						'minutes' => timeleft($astro_moon_new, false, false, true)
					]
				],
				'next_new' => [
					'timestamp' => $astro_moon_nextnew,
					'date' => date_($astro_moon_nextnew, 'date'),
					'time' => arr_time($astro_moon_nextnew),
					'countdown' => [
						'days' => timeleft($astro_moon_nextnew, true),
						'hours' => timeleft($astro_moon_nextnew, false, true),
						'minutes' => timeleft($astro_moon_nextnew, false, false, true)
					]
				]
			]
		];



		$arr_astromoo = [
			'sun' => $arr_astro_sun,
			'moon' => $arr_astro_moon
		];
	}

?>