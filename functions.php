<?php

	function convert($value, $method) {
		if($method == 'f') {
			return ($value * 9/5) + 32;

		} elseif($method == 'k') {
			return ($value + 274.15);

		} elseif($method == 'cm') {
			return ($value / 10);

		} elseif($method == 'in') {
			return ($value / 25.4);

		} elseif($method == 'pa') {
			return ($value * 100);

		} elseif($method == 'bar') {
			return ($value * 0.000999922256);

		} elseif($method == 'mmhg') {
			return ($value * 0.75006157584566);

		} elseif($method == 'inhg') {
			return ($value * 0.02952998751);

		} elseif($method == 'psi') {
			return ($value * 0.01450377377);

		} elseif($method == 'atm') {
			return ($value * 0.9790278806);

		} elseif($method == 'km') {
			return ($value * 0.001);

		} elseif($method == 'mi') {
			return ($value * 0.00062137119);

		} elseif($method == 'kmh') {
			return ($value * 3.6);

		} elseif($method == 'mph') {
			return ($value * 0.44704);

		} elseif($method == 'knot') {
			return ($value * 1.9438444924406);

		} elseif($method == 'percent') {
			return ($value * 100);
		}
	}

	function arr_date($value) {
		return [
			'year' => (int)date('Y', $value),
			'month' => (int)date('n', $value),
			'day' => (int)date('j', $value)
		];
	}

	function arr_time($value) {
		return [
			'hours' => [
				'twentyfour' => (int)date('H', $value),
				'twelve' => (int)date('h', $value)
			],
			'minutes' => (int)date('i', $value),
			'ampm' => date('A', $value)
		];
	}

	function beaufort($string) {
		if($string == 0) {
			$value = '0';
		} elseif($string >= 1 OR $string <= 3) {
			$value = '1';
		} elseif($string >= 4 OR $string <= 6) {
			$value = '2';
		} elseif($string >= 7 OR $string <= 10) {
			$value = '3';
		} elseif($string >= 11 OR $string <= 16) {
			$value = '4';
		} elseif($string >= 17 OR $string <= 21) {
			$value = '5';
		} elseif($string >= 22 OR $string <= 27) {
			$value = '6';
		} elseif($string >= 28 OR $string <= 33) {
			$value = '7';
		} elseif($string >= 34 OR $string <= 40) {
			$value = '8';
		} elseif($string >= 41 OR $string <= 47) {
			$value = '9';
		} elseif($string >= 48 OR $string <= 55) {
			$value = '10';
		} elseif($string >= 56 OR $string <= 63) {
			$value = '11';
		} elseif($string >= 64 OR $string <= 71) {
			$value = '12';
		}

		return (int)$value;
	}



	function weathericon($string) {
		$icon_prefix = 'pe-7w-';
		$array = Array(
			'01d800' => 'sun',
			'01n800' => 'moon',

			'02d801' => 'cloud-sun',
			'02n801' => 'cloud-moon',

			'03d802' => 'cloud-sun',
			'03n802' => 'cloud-moon',

			'04d803' => 'cloud',
			'04d804' => 'cloud',
			'04n803' => 'cloud',
			'04n804' => 'cloud',

			'09d300' => 'drizzle-alt',
			'09d301' => 'drizzle',
			'09d302' => 'drizzle',
			'09d310' => 'drizzle-alt',
			'09d311' => 'drizzle',
			'09d312' => 'drizzle',
			'09d313' => 'drizzle',
			'09d314' => 'drizzle',
			'09d321' => 'drizzle',
			'09d520' => 'rain-alt',
			'09d521' => 'rain-alt',
			'09d522' => 'rain-alt',
			'09d531' => 'rain-alt',
			'09n300' => 'drizzle-alt',
			'09n301' => 'drizzle',
			'09n302' => 'drizzle',
			'09n310' => 'drizzle-alt',
			'09n311' => 'drizzle',
			'09n312' => 'drizzle',
			'09n313' => 'drizzle',
			'09n314' => 'drizzle',
			'09n321' => 'drizzle',
			'09n520' => 'rain-alt',
			'09n521' => 'rain-alt',
			'09n522' => 'rain-alt',
			'09n531' => 'rain-alt',

			'10d500' => 'rain',
			'10d501' => 'rain',
			'10d502' => 'rain',
			'10d503' => 'rain-alt',
			'10d504' => 'rain-alt',
			'10n500' => 'rain',
			'10n501' => 'rain',
			'10n502' => 'rain',
			'10n503' => 'rain-alt',
			'10n504' => 'rain-alt',

			'11d200' => 'lightning-rain',
			'11d201' => 'lightning-rain',
			'11d202' => 'lightning-rain',
			'11d210' => 'lightning',
			'11d211' => 'lightning',
			'11d212' => 'lightning',
			'11d221' => 'lightning',
			'11d230' => 'lightning-rain',
			'11d231' => 'lightning-rain',
			'11d232' => 'lightning-rain',
			'11n200' => 'lightning-rain',
			'11n201' => 'lightning-rain',
			'11n202' => 'lightning-rain',
			'11n210' => 'lightning',
			'11n211' => 'lightning',
			'11n212' => 'lightning',
			'11n221' => 'lightning',
			'11n230' => 'lightning-rain',
			'11n231' => 'lightning-rain',
			'11n232' => 'lightning-rain',

			'13d511' => 'rain',
			'13d600' => 'snow-alt',
			'13d601' => 'snow-alt',
			'13d602' => 'snow-alt',
			'13d611' => 'snow',
			'13d612' => 'snow',
			'13d613' => 'snow',
			'13d615' => 'snow',
			'13d616' => 'snow',
			'13d620' => 'snow',
			'13d621' => 'snow',
			'13d622' => 'snow',
			'13n511' => 'rain',
			'13n600' => 'snow-alt',
			'13n601' => 'snow-alt',
			'13n602' => 'snow-alt',
			'13n611' => 'snow',
			'13n612' => 'snow',
			'13n613' => 'snow',
			'13n615' => 'snow',
			'13n616' => 'snow',
			'13n620' => 'snow',
			'13n621' => 'snow',
			'13n622' => 'snow',

			'50d701' => 'fog',
			'50d711' => 'fog',
			'50d721' => 'fog',
			'50d731' => 'fog',
			'50d741' => 'fog',
			'50d751' => 'fog',
			'50d761' => 'fog',
			'50d762' => 'fog',
			'50d771' => 'fog',
			'50d781' => 'hurricane',
			'50n701' => 'fog',
			'50n711' => 'fog',
			'50n721' => 'fog',
			'50n731' => 'fog',
			'50n741' => 'fog',
			'50n751' => 'fog',
			'50n761' => 'fog',
			'50n762' => 'fog',
			'50n771' => 'fog',
			'50n781' => 'hurricane'
		);

		return $icon_prefix . (!isset($array[$string]) ? '-' : $array[$string]);
	}

	function wind_descriptions($variable, $type) {
		if($variable == 0 AND $variable <= 0.2) {
			$wind_desc_land = 'Calm';
			$wind_desc_sea = 'Lull';

		} elseif($variable >= 0.3 AND $variable <= 3.3) {
			$wind_desc_land = 'Calm';
			$wind_desc_sea = 'Breeze';

		} elseif($variable >= 3.4 AND $variable <= 7.9) {
			$wind_desc_land = 'Moderate';
			$wind_desc_sea = 'Breeze';

		} elseif($variable >= 8.0 AND $variable <= 13.8) {
			$wind_desc_land = 'Fresh breeze';
			$wind_desc_sea = 'Breeze';

		} elseif($variable >= 13.9 AND $variable <= 24.4) {
			$wind_desc_land = 'Strong';
			$wind_desc_sea = 'Gale';

		} elseif($variable >= 24.5 AND $variable <= 32.6) {
			$wind_desc_land = 'Storm';
			$wind_desc_sea = 'Storm';

		} elseif($variable >= 32.7) {
			$wind_desc_land = 'Hurricane';
			$wind_desc_sea = 'Hurricane';

		} else {
			$wind_desc_land = null;
			$wind_desc_sea = null;
		}


		return ($type == 'land' ? $wind_desc_land : ($type == 'sea' ? $wind_desc_sea : null));
	}

	function air_pollution_quality($number, $type) {
		$value = null;
		if($number >= 0 AND $number <= 50) {
			$value = ($type == 'grade' ? 'good' : ($type == 'colour' ? 'green' : null));

		} elseif($number >= 51 AND $number <= 100) {
			$value = ($type == 'grade' ? 'moderate' : ($type == 'colour' ? 'yellow' : null));

		} elseif($number >= 101 AND $number <= 150) {
			$value = ($type == 'grade' ? 'unhealthy_certain_people' : ($type == 'colour' ? 'orange' : null));

		} elseif($number >= 151 AND $number <= 200) {
			$value = ($type == 'grade' ? 'unhealthy' : ($type == 'colour' ? 'red' : null));

		} elseif($number >= 201 AND $number <= 300) {
			$value = ($type == 'grade' ? 'very_unhealthy' : ($type == 'colour' ? 'violet' : null));

		} elseif($number >= 300) {
			$value = ($type == 'grade' ? 'hazardous' : ($type == 'colour' ? 'dark_red' : null));

		} else {
			$value = 'unknown';
		}

		return $value;
	}

	function timeleft($to, $viewdays = true, $viewhours = false, $viewminutes = false) {
		$dt_current = new DateTime();
		$dt_future = new DateTime(date_($to, 'datetime'));
		$daysleft = ($dt_future->diff($dt_current)->invert == 0 ? 0 : $dt_future->diff($dt_current)->d);
		$hoursleft = ($dt_future->diff($dt_current)->invert == 0 ? 0 : $dt_future->diff($dt_current)->h);
		$minutesleft = ($dt_future->diff($dt_current)->invert == 0 ? 0 : $dt_future->diff($dt_current)->i);

		if($viewdays == true) {
			return (int)$daysleft;
		} elseif($viewhours == true) {
			return (int)$hoursleft;
		} elseif($viewminutes == true) {
			return (int)$minutesleft;
		}
	}

	function degrees_to_compass($num) {
		$val = (($num / 22.5) + 0.5);
		$arr = [
			'N', 'NNE', 'NE', 'NW', 'NNW',
			'ENE', 'E', 'ESE',
			'SE', 'SSE', 'S', 'SSW', 'SW',
			'WSW', 'W', 'WNW',
		];

		return $arr[($val % 16)];
	}

	function date_($timestamp, $format) {
		global $lang;


		if($format == 'date') {
			return date('Y-m-d', $timestamp);

		} elseif($format == 'datetime') {
			return date('Y-m-d, H:i', $timestamp);

		} elseif($format == 'datetime-s') {
			return date('Y-m-d, H:i:s', $timestamp);

		} elseif($format == 'time-24h') {
			return date('H:i', $timestamp);

		} elseif($format == 'time-24h-s') {
			return date('H:i:s', $timestamp);

		} elseif($format == 'time-12h') {
			return date('g:i A', $timestamp);

		} elseif($format == 'time-12h-s') {
			return date('g:i:s A', $timestamp);

		} elseif($format == 'year') {
			return date('Y', $timestamp);

		} elseif($format == 'month') {
			return $months[date('n', $timestamp)];

		} elseif($format == 'month-short') {
			return $months_short[$timestamp];

		} elseif($format == 'year-month') {
			return mb_strtolower($months[date('n', strtotime($timestamp))]).', '.date('Y', strtotime($timestamp));

		} elseif($format == 'year-month-day') {
			return date('j', strtotime($timestamp)).' '.mb_strtolower($months[date('n', strtotime($timestamp))]).', '.date('Y', strtotime($timestamp));

		} elseif($format == 'detailed') {
			return date('j', $timestamp).' '.mb_strtolower($months[date('n', $timestamp)]).', '.date('Y', $timestamp).', kl. '.date('H:i', $timestamp);
		}
	}

	function format_number($string, $decimals = '1', $seperator = ',', $nospace = false) {
		$string = number_format($string, $decimals, $seperator, ($nospace == false ? ' ' : ''));
		$explode = explode($seperator, $string);

		if($seperator == ',' OR $seperator == '.') {
			if($nospace == false) {
				return $explode[0] . (!isset($explode[1]) ? null : ($explode[1] == 0 ? null : $seperator . $explode[1]));
			} else {
				return (float)$string;
			}

		} else {
			return '0';
		}
	}

	function abs_diff($v1, $v2) {
		$diff = $v1 - $v2;
		return $diff < 0 ? '+'.((-1) * $diff) : '-'.$diff;
	}

	function calc_distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
		$latFrom = deg2rad($latitudeFrom);
		$lonFrom = deg2rad($longitudeFrom);
		$latTo = deg2rad($latitudeTo);
		$lonTo = deg2rad($longitudeTo);

		$lonDelta = $lonTo - $lonFrom;
		$a = pow(cos($latTo) * sin($lonDelta), 2) +
		pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
		$b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

		$angle = atan2(sqrt($a), $b);
		return $angle * $earthRadius;
	}

	function calc_h2o_perkg_air($humidity, $temp) {
		return ($humidity * 0.42 * exp($temp * 10 * 0.006235398) / 10);
	}

	function uvindex_grade($number, $type) {
		$arr = [
			0 => [
				"risk" => "low",
				"colour" => "green"
			],
			1 => [
				"risk" => "low",
				"colour" => "green"
			],
			2 => [
				"risk" => "low",
				"colour" => "green"
			],
			3 => [
				"risk" => "moderate",
				"colour" => "yellow"
			],
			4 => [
				"risk" => "moderate",
				"colour" => "yellow"
			],
			5 => [
				"risk" => "moderate",
				"colour" => "yellow"
			],
			6 => [
				"risk" => "high",
				"colour" => "orange"
			],
			7 => [
				"risk" => "high",
				"colour" => "orange"
			],
			8 => [
				"risk" => "very_high",
				"colour" => "red"
			],
			9 => [
				"risk" => "very_high",
				"colour" => "red"
			],
			10 => [
				"risk" => "very_high",
				"colour" => "red"
			],
			11 => [
				"risk" => "extreme",
				"colour" => "violet"
			]
		];

		if($number > 11) {
			return $arr[11][$type];
		} else {
			return $arr[$number][$type];
		}
	}

?>
