<?php

	function url($string, $pure = true) {
		global $is_locally;

		$basename_dir = basename(__DIR__) . (($pure == false AND $string == '') ? '' : '/');
		return '/'.($is_locally == true ? $basename_dir : basename(__DIR__).'/') . $string;
	}

	function svgicon($string) {
		$arr = [
			'anchor' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M17.657 14.828l-1.414-1.414L17.657 12A4 4 0 1 0 12 6.343l-1.414 1.414-1.414-1.414 1.414-1.414a6 6 0 0 1 8.485 8.485l-1.414 1.414zm-2.829 2.829l-1.414 1.414a6 6 0 1 1-8.485-8.485l1.414-1.414 1.414 1.414L6.343 12A4 4 0 1 0 12 17.657l1.414-1.414 1.414 1.414zm0-9.9l1.415 1.415-7.071 7.07-1.415-1.414 7.071-7.07z"/></svg>'
		];

		return $arr[$string];
	}

	function link_($string, $url) {
		return '<a href="'.$url.'" rel="noreferer" target="_blank">'.$string.'</a>';
	}

	function h3($string, $anchor, $isfirst = false) {
		return '<h3'.($isfirst == false ? null : ' class="first"').' id="'.$anchor.'"><a href="#'.$anchor.'">'.svgicon('anchor').'</a>'.$string.'</h3>';
	}

	function anchor($string, $anchor) {
		return '<a href="#'.$anchor.'">'.$string.'</a>';
	}

	function code($string) {
		return '<code>'.$string.'</code>';
	}

	function attention($code, $content) {
		if($code == 'info') {
			return '<div class="attention"><div class="code info">Info</div><div class="content">'.$content.'</div></div>';

		} elseif($code == 'important') {
			return '<div class="attention"><div class="code important">Important</div><div class="content">'.$content.'</div></div>';
		}
	}



	$og_title = 'Serenum API Wiki';
	$og_url = 'https://wiki.serenum.org';
	$og_image = null;
	$og_image_alt = null;
	$og_description = null;
	$servicename = 'Serenum API';

	$arr_fields = [
		[
			'parameter' => code('apikey'),
			'description' => 'Shows the entered API key.'
		],
		[
			'parameter' => code('latitude'),
			'description' => 'Geographical coordinates of the place in latitude (truncated to 5 digits).'
		],
		[
			'parameter' => code('longitude'),
			'description' => 'Geographical coordinates of the place in longitude (truncated to 5 digits).'
		],
		[
			'parameter' => code('*').' . '.code('twentyfourhours'),
			'description' => 'Shows the time in 24h format for the chosen place.'
		],
		[
			'parameter' => code('*').' . '.code('twelvehours'),
			'description' => 'Shows the time in 12h format for the chosen place.'
		],
		[
			'parameter' => code('*').' . '.code('clouds'),
			'description' => 'Cloud cover in percentage (%)'
		],
		[
			'parameter' => code('*').' . '.code('humidity'),
			'description' => 'Humidity in percentage (%)'
		],
		[
			'parameter' => code('prob_precip'),
			'description' => 'The probability of precipitation in percentage (%)'
		],
		[
			'parameter' => code('moon').' . '.code('schedules').' . '.code('*').' . '.code('countdown').' . '.code('*'),
			'description' => 'How many days, hours, and minutes there are left until the moon phase occures. The countdown goes after the chosen place\'s date and time.'
		]
	];

	$arr_errors = [
		[
			'code' => 110,
			'description' => 'The geographical coordinates (latitude and longitude) are missing in the URL. You need to specify these in order to get the data from OpenWeatherMap. Please see "<a href="'.url('wiki#get-weather-data').'">Get the weather data</a>" for how to get the coordinates for a place.'
		],
		[
			'code' => 140,
			'description' => 'The geographical coordinates (latitude and longitude) are not valid. To get a valid latitude och longitude, they need to be 5 or more digits and with '.code('-').' and/or '.code('.').'. You can get one from OpenStreetMap (see "<a href="'.url('wiki#get-weather-data').'">Get the weather data</a>" to learn how).'
		],
		[
			'code' => 150,
			'description' => 'The API key from OpenWeatherMap are missing in the URL. You need to login to your account at '.link_('openweathermap.org', 'https://openweathermap.org').' and retrieve your API key from there. See "<a href="'.url('wiki#get-api-key').'">Get the API key</a>" for more information.'
		],
		[
			'code' => 160,
			'description' => 'The API key from OpenWeatherMap are not valid. Go to '.link_('openweathermap.org', 'https://openweathermap.org').' and login to your account and either create a new API key or confirm that the entered API key are activated.'
		],
		[
			'code' => 170,
			'description' => 'At least 1 excluded variable are missing in the URL. See "<a href="'.url('wiki#get-started').'">Get started</a>" for more information.'
		],
		[
			'code' => 200,
			'description' => 'The API were not able to fetch the data from OpenWeatherMap. This can occur if the API key from OSM are not valid based on their standards or not activated.'
		]
	];



	echo '<!DOCTYPE html>';
	echo '<html>';
		echo '<head>';
			echo '<title>';
				echo $og_title;
			echo '</title>';

			echo '<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">';
			echo '<meta name="robots" content="noindex, nofollow">';
			echo '<meta name="google" content="notranslate">';

			echo '<meta property="og:title" content="'.$og_title.'">';
			echo '<meta property="og:site_name" content="'.$og_title.'">';
			echo '<meta property="og:type" content="website">';
			echo '<meta property="og:url" content="'.(isset($og_url) ? '-' : $og_url).'">';
			echo '<meta property="og:image" content="'.($og_image == null ? '-' : $og_image).'?s=512">';
			echo '<meta property="og:image:secure_url" content="'.($og_image == null ? '-' : $og_image).'?s=512">';
			echo '<meta property="og:image:type" content="image/jpeg">';
			echo '<meta property="og:image:width" content="512">';
			echo '<meta property="og:image:height" content="512">';
			echo '<meta property="og:image:alt" content="'.($og_image_alt == null ? '-' : $og_image_alt).'">';
			echo '<meta property="og:description" content="'.($og_description == null ? '-' : $og_description).'">';
			echo '<meta property="og:locale" content="sv_SE">';

			echo '<meta name="twitter:card" content="summary"></meta>';

			echo '<link rel="canonical" href="'.(isset($og_url) ? '-' : $og_url).'">';
			echo '<link href="'.(isset($og_url) ? '-' : $og_url).'" rel="me">';

			echo '<link href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQEAYAAABPYyMiAAAABmJLR0T///////8JWPfcAAAACXBIWXMAAABIAAAASABGyWs+AAAAF0lEQVRIx2NgGAWjYBSMglEwCkbBSAcACBAAAeaR9cIAAAAASUVORK5CYII=" rel="icon" type="image/x-icon">';



			echo '<link type="text/css" rel="stylesheet" href="wiki.css?'.time().'">';
		echo '</head>';
		echo '<body>';



			echo '<main>';
				echo '<h2>';
					echo '<a href="'.url('wiki').'">'.$og_title.'</a>';
				echo '</h2>';

				echo '<section id="roadmap">';
					echo '<details>';
						echo '<summary>Show or hide the roadmap</summary>';

						echo anchor('Introduction', 'intro');
						echo anchor('Download the files', 'download');
						echo anchor('Get the API key', 'get-api-key');
						echo anchor('Get the weather data', 'get-weather-data');
						echo anchor('Get started', 'get-started');
						echo anchor('Error codes', 'error-codes');
						echo anchor('Issues? Contact us', 'contact');
					echo '</details>';
				echo '</section>';


				# Intro
				echo h3('Introduction', 'intro', true);
				echo '<p>With Serenum API, you will get a detailed overview over the current weather and weather forecast for a place. You will also get detailed information about a place\'s local date and time, including astronomical data like the current positions of the Sun and the Moon. And the best part: Serenum API are completely open sourced and you can install it on your own server!</p>';
				echo '<p>The API are divided in 2 sections: getting all the data about a single place ('.code('index.php').'), and see the difference between 2 places ('.code('diff.php').').</p>';


				# Download the files
				echo h3('Download the files', 'download');
				echo '<ol>';
					echo '<li>Clone '.link_('the repository at Codeberg', 'https://codeberg.org/serenum/api').' or download the ZIP or TAR.GZ file from the same place.</li>';
					echo '<li>If you downloaded the compressed file, extract the files to the root of your web server.</li>';
					echo '<li>Read the next chapter about how to get API key.</li>';
				echo '</ol>';


				# Get API key
				echo h3('Get the API key', 'get-api-key');
				echo '<p>Before you can use '.$servicename.', you must get your own API key from '.link_('OpenWeatherMap', 'https://openweathermap.org').'.</p>';

				echo '<ol>';
					echo '<li>Create an account if you don\'t already have one at openweathermap.org.</li>';
					echo '<li>When you\'ve logged in, choose "API key" in the top menu. If you don\'t have any keys, create one below "Create key".</li>';
					echo '<li>Method 1: Copy the API key and replace it with '.code('[your-api-key]').' for the variable '.code('$apikey').' in both '.code('index.php').' and '.code('diff.php').'.</li>';
					echo '<li>Method 2: Append '.code('&key=[your-api-key]').' to '.code('domain.com/api?coor=59.38079,13.50239').' in your browser.</li>';
				echo '</ol>';

				echo attention('important', 'If you have created a new key or reactivated a key that has been inactivated, it can take some time until you can use the key.');


				# Get the weather data
				echo h3('Get the weather data', 'get-weather-data');
				echo '<p>Now go to '.code('https://domain.com/api').' and follow the instructions which will say that you need to append the coordinates for the place you want to fetch the weather data for. You can get the coordinates for the place by go to '.link_('OpenStreetMap', 'https://openstreetmap.org').' or use the default ones that the API gives you.</p>';
				echo '<p>If you choose to get coordinates from OSM, go to the place of choice and right-click on the place and choose "Centre map here". Now copy the coordinates from the URL and append them to '.code('domain.com/api').' or '.code('domain.com/api/diff').'. Follow the example in Serenum API for how it must look like.</p>';
				echo attention('important', 'Be sure to replace '.code('/').' with '.code(',').' for the coordinates before you continue.');


				# Get started
				echo h3('Get started', 'get-started');
				echo '<p>'.$servicename.' only provides data in JSON.</p>';

				echo '<pre>';
					echo 'https://domain.com/?coor=<code>[latitude]</code>,<code>[longitude]</code>&key=<code>[apikey]</code>';
				echo '</pre>';

				echo '<pre class="space-below">';
					echo 'https://domain.com/diff?coor=<code>[latitude]</code>,<code>[longitude]</code>;<code>[latitude]</code>,<code>[longitude]</code>&key=<code>[apikey]</code>';
				echo '</pre>';


				echo '<table>';
					echo '<thead>';
						echo '<tr>';
							echo '<td class="parameter">Parameter</td>';
							echo '<td class="required">Required</td>';
							echo '<td class="description">Description</td>';
						echo '</tr>';
					echo '</thead>';


					echo '<tbody>';
						echo '<tr>';
							echo '<td>';
								echo '<code>latitude</code> & <code>longitude</code>';
							echo '</td>';

							echo '<td><div class="portable">Required</div><div class="desktop">Yes</div></td>';
							echo '<td>Geographical coordinates.</td>';
						echo '</tr>';

						echo '<tr>';
							echo '<td>';
								echo '<code>apikey</code>';
							echo '</td>';

							echo '<td><div class="portable">Required</div><div class="desktop">Yes</div></td>';
							echo '<td>Your own API key from OpenWeatherMap.</td>';
						echo '</tr>';

						echo '<tr>';
							echo '<td>';
								echo '<code>exclude</code>';
							echo '</td>';

							echo '<td><div class="portable">Not required</div><div class="desktop">No</div></td>';
							echo '<td>';
								echo 'Exclude stuff from the API call, seperated with a comma and no spaces. Available values: <code>place</code>, <code>pollen</code>, <code>airp</code>, <code>astronomy</code>.';
							echo '</td>';
						echo '</tr>';
					echo '</tbody>';
				echo '</table>';



				echo '<h4>Fields in the API response</h4>';

				echo '<div class="intro">';
					echo '<p>Despite the API itself are very self-explainable, there are fields that aren\'t and those are listed below. Other fields that needs more information are included.</p>';
					echo '<p>Fields with the same name that can be found on more than 1 place, are listed with a wildcard (*).</p>';
				echo '</div>';

				echo '<table id="fields">';
					echo '<tbody>';
						foreach($arr_fields AS $field) {
							echo '<tr>';
								echo '<td class="parameter">';
									echo $field['parameter'];
								echo '</td>';

								echo '<td class="description">';
									echo $field['description'];
								echo '</td>';
							echo '</tr>';
						}
					echo '</tbody>';
				echo '</table>';

				echo attention('info', 'The astronomical values are only in beta! We\'ve seen that the sun hours and moon hours are not the same as on timeanddate.com. We are working on a solution.');


				# Error codes
				echo h3('Error codes', 'error-codes');
				echo '<p>When an error occurs in Serenum API, you will get the error code, a message saying what the error means in short, and (depending on the error) 1 or 2 other variable(s) that will help you getting on the right path. All errors are listed below with further explaination.</p>';

				echo '<table id="errors">';
					echo '<thead>';
						echo '<tr>';
							echo '<td class="code">Error code</td>';
							echo '<td class="description">Description</td>';
						echo '</tr>';
					echo '</thead>';


					echo '<tbody>';
						foreach($arr_errors AS $error) {
							echo '<tr>';
								echo '<td class="code">';
									echo '<div class="portable">Error '.$error['code'].'</div>';
									echo '<div class="desktop">'.$error['code'].'</div>';
								echo '</td>';

								echo '<td class="description">';
									echo $error['description'];
								echo '</td>';
							echo '</tr>';
						}
					echo '</tbody>';
				echo '</table>';


				# Contact us
				echo h3('Issues? Contact us', 'contact');
				echo '<p>If you have stumbled upon any issues, feel free to contact us on either '.link_('Codeberg', 'https://codeberg.org/serenum/api').' or '.link_('Telegram', 'https://t.me/serenumfoss').'.</p>';
			echo '</main>';



		echo '</body>';
	echo '</html>';

?>
