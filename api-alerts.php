<?php

	$arr_alerts = [];
	if(isset($data_weather->alerts)) {
		foreach($data_weather->alerts AS $alert) {
			$weather_a_start = (!isset($alert->start) ? null : $alert->start);
			$weather_a_end = (!isset($alert->end) ? null : $alert->end);
			$weather_a_event = (!isset($alert->event) ? null : $alert->event);
			$weather_a_sender = (!isset($alert->sender_name) ? null : $alert->sender_name);
			$weather_a_description = (!isset($alert->description) ? null : $alert->description);

			$arr_tags = [];
			foreach($alert->tags AS $tag) {
				$arr_tags[] = $tag;
			}

			$arr_alerts[] = [
				'timestamps' => [
					'start' => $weather_a_start,
					'end' => $weather_a_end
				],
				'event' => $weather_a_event,
				'sender' => $weather_a_sender,
				'description' => $weather_a_description,
				'tags' => $arr_tags
			];
		}
	}

?>