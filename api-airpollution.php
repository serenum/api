<?php

	if(!in_array('airp', $arr_excluded)) {
		$arr_airpollution = [
			'quality' => [
				"grade" => air_pollution_quality((int)$airpollution_aqi, 'grade'),
				"colour" => air_pollution_quality((int)$airpollution_aqi, 'colour')
			],
			'aqi' => (int)$airpollution_aqi,
			'co' => (float)$airpollution_co,
			'no' => (float)$airpollution_no,
			'no2' => (float)$airpollution_no2,
			'o3' => (float)$airpollution_o3,
			'so2' => (float)$airpollution_so2,
			'pm25' => (float)$airpollution_pm2_5,
			'pm10' => (float)$airpollution_pm10,
			'nh3' => (float)$airpollution_nh3
		];
	}

?>