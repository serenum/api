<?php

	$arr_wea_hourly = [];
	foreach($data_weather->hourly AS $hourly) {
		$weather_dt_data = (!isset($hourly->dt) ? null : $hourly->dt);
		$weather_h_id = (!isset($hourly->weather[0]->id) ? null : $hourly->weather[0]->id);
		$weather_h_ic = (!isset($hourly->weather[0]->icon) ? null : $hourly->weather[0]->icon);
		$weather_h_wm = (!isset($hourly->weather[0]->main) ? null : $hourly->weather[0]->main);
		$weather_h_wd = (!isset($hourly->weather[0]->description) ? null : $hourly->weather[0]->description);
		$weather_h_p = (!isset($hourly->pop) ? null : convert($hourly->pop, 'percent'));
		$weather_h_r_mm = (!isset($hourly->rain->{'1h'}) ? null : $hourly->rain->{'1h'});
		$weather_h_r_cm = ($weather_h_r_mm == null ? null : convert($weather_h_r_mm, 'cm'));
		$weather_h_r_in = ($weather_h_r_mm == null ? null : convert($weather_h_r_mm, 'in'));
		$weather_h_s_mm = (!isset($hourly->snow->{'1h'}) ? null : $hourly->snow->{'1h'});
		$weather_h_s_cm = ($weather_h_s_mm == null ? null : convert($weather_h_s_mm, 'cm'));
		$weather_h_s_in = ($weather_h_s_mm == null ? null : convert($weather_h_s_mm, 'in'));

		$weather_h_t_c = (!isset($hourly->temp) ? null : $hourly->temp);
		$weather_h_t_f = (!isset($hourly->temp) ? null : convert($weather_h_t_c, 'f'));
		$weather_h_t_k = (!isset($hourly->temp) ? null : convert($weather_h_t_c, 'k'));
		$weather_h_tfl_c = (!isset($hourly->feels_like) ? null : $hourly->feels_like);
		$weather_h_tfl_f = (!isset($hourly->feels_like) ? null : convert($weather_h_tfl_c, 'f'));
		$weather_h_tfl_k = (!isset($hourly->feels_like) ? null : convert($weather_h_tfl_c, 'k'));
		$weather_h_tdp_c = (!isset($hourly->dew_point) ? null : $hourly->dew_point);
		$weather_h_tdp_f = (!isset($hourly->dew_point) ? null : convert($weather_h_tdp_c, 'f'));
		$weather_h_tdp_k = (!isset($hourly->feels_like) ? null : convert($weather_h_tdp_c, 'k'));
		$weather_h_ws_ms = (!isset($hourly->wind_speed) ? null : $hourly->wind_speed);
		$weather_h_ws_g_ms = (!isset($hourly->wind_gust) ? null : $hourly->wind_gust);
		$weather_h_ws_kmh = ($weather_h_ws_ms == null ? null : convert($weather_h_ws_ms, 'kmh'));
		$weather_h_ws_g_kmh = ($weather_h_ws_g_ms == null ? null : convert($weather_h_ws_g_ms, 'kmh'));
		$weather_h_ws_mph = ($weather_h_ws_ms == null ? null : convert($weather_h_ws_ms, 'mph'));
		$weather_h_ws_g_mph = ($weather_h_ws_g_ms == null ? null : convert($weather_h_ws_g_ms, 'mph'));
		$weather_h_ws_kt = ($weather_h_ws_ms == null ? null : convert($weather_h_ws_ms, 'knot'));
		$weather_h_ws_g_kt = ($weather_h_ws_g_ms == null ? null : convert($weather_h_ws_g_ms, 'knot'));
		$weather_h_wd_deg = (!isset($hourly->wind_deg) ? null : $hourly->wind_deg);
		$weather_h_c = (!isset($hourly->clouds) ? null : $hourly->clouds);
		$weather_h_uvi = (!isset($hourly->uvi) ? null : $hourly->uvi);
		$weather_h_pr = (!isset($hourly->pressure) ? null : $hourly->pressure);
		$weather_h_pr_pa = (!isset($hourly->pressure) ? null : convert($weather_h_pr, 'pa'));
		$weather_h_pr_bar = (!isset($hourly->pressure) ? null : convert($weather_h_pr, 'bar'));
		$weather_h_pr_mmhg = (!isset($hourly->pressure) ? null : convert($weather_h_pr, 'mmhg'));
		$weather_h_pr_inhg = (!isset($hourly->pressure) ? null : convert($weather_h_pr, 'inhg'));
		$weather_h_pr_psi = (!isset($hourly->pressure) ? null : convert($weather_h_pr, 'psi'));
		$weather_h_pr_atm = (!isset($hourly->pressure) ? null : convert($weather_h_pr, 'atm'));
		$weather_h_h = (!isset($hourly->humidity) ? null : $hourly->humidity);
		$weather_h_h20perkgofair = ($weather_h_h == null ? null : calc_h2o_perkg_air($weather_h_h, $weather_h_t_c));
		$weather_h_v_m = (!isset($hourly->visibility) ? null : $hourly->visibility);
		$weather_h_v_km = (!isset($hourly->visibility) ? null : convert($weather_h_v_m, 'km'));
		$weather_h_v_mi = (!isset($hourly->visibility) ? null : convert($weather_h_v_m, 'mi'));

		$correct_hour = new DateTime(date('Y-m-d H:i:s', $weather_dt_data));
		$correct_hour->setTimeZone(new DateTimeZone($timezone));
		$weather_dt_data = strtotime($correct_hour->format('Y-m-d H:i:s'));

		$astro = new AurorasLive\SunCalc($correct_hour, $latitude, $longitude);
		$astro_sun_times = $astro->getSunTimes();
		$astro_sun_position = $astro->getSunPosition();
		$astro_moon_position = $astro->getMoonPosition($correct_hour);
		$astro_moon_illumination = $astro->getMoonIllumination($correct_hour);
		$astro_moon_times = $astro->getMoonTimes($correct_hour);

		$astro_sun_altitude = ($astro_sun_position->altitude * 180 / M_PI);
		$astro_sun_azimuth = ($astro_sun_position->azimuth * 180 / M_PI);
		$astro_sun_isup = ($astro_sun_altitude < 0 ? false : true);
		$astro_sun_rise = ($astro_sun_times['sunrise'] == null ? null : strtotime($astro_sun_times['sunrise']->format('Y-m-d H:i:s')));
		$astro_sun_set = ($astro_sun_times['sunset'] == null ? null : strtotime($astro_sun_times['sunset']->format('Y-m-d H:i:s')));

		$astro_moon_altitude = ($astro_moon_position->altitude * 180 / M_PI);
		$astro_moon_azimuth = ($astro_moon_position->azimuth * 180 / M_PI);
		$astro_moon_rise = (!isset($astro_moon_times['moonrise']) ? null : strtotime($astro_moon_times['moonrise']->format('Y-m-d H:i:s')));
		$astro_moon_set = (!isset($astro_moon_times['moonset']) ? null : strtotime($astro_moon_times['moonset']->format('Y-m-d H:i:s')));
		$astro_moon_age = $astro_moon->get('age');
		$astro_moon_phase = $astro_moon_illumination['phase'];
		$astro_moon_phase_name = $astro_moon->phase_name();
		$astro_moon_fraction = $astro_moon_illumination['fraction'];
		$astro_moon_angle = $astro_moon_illumination['angle'];
		$astro_moon_distance_earth = $astro_moon->get('distance');
		$astro_moon_distance_sun = $astro_moon->get('sundistance');
		$astro_moon_diameter_moon = $astro_moon->get('diameter');
		$astro_moon_diameter_sun = $astro_moon->get('sundiameter');

		$daylight_rise = new DateTime(date_($astro_sun_rise, 'datetime'), new DateTimeZone($timezone));
		$daylight_set = new DateTime(date_($astro_sun_set, 'datetime'), new DateTimeZone($timezone));
		$daylight = $daylight_set->diff($daylight_rise);

		$night_starts = new DateTime(date_($astro_sun_night, 'datetime'), new DateTimeZone($timezone));
		$night_ends = new DateTime(date_($astro_sun_rise, 'datetime'), new DateTimeZone($timezone));
		$night = $night_starts->diff($night_ends);



		$arr_wea_hourly[] = [
			'timestamp' => $weather_dt_data,
			'date' => date_($weather_dt_data, 'date'),
			'time' => arr_time($weather_dt_data),
			'weather' => [
				'id' => $weather_h_id,
				'id_icon' => $weather_h_ic,
				'icon' => weathericon($weather_h_ic . $weather_h_id),
				'main' => $weather_h_wm,
				'description' => $weather_h_wd,
				'prob_precip' => (float)format_number($weather_h_p, 0),
				'rain' => [
					'mm' => (float)format_number($weather_h_r_mm, 2, '.'),
					'cm' => (float)format_number($weather_h_r_cm, 2, '.'),
					'in' => (float)format_number($weather_h_r_in, 2, '.')
				],
				'snow' => [
					'mm' => (float)format_number($weather_h_s_mm, 2, '.'),
					'cm' => (float)format_number($weather_h_s_cm, 2, '.'),
					'in' => (float)format_number($weather_h_s_in, 2, '.')
				]
			],
			'temperature' => [
				'celcius' => (float)format_number($weather_h_t_c, 2, '.'),
				'fahrenheit' => (float)format_number($weather_h_t_f, 2, '.'),
				'kelvin' => (float)format_number($weather_h_t_k, 2, '.')
			],
			'temperature_feels_like' => [
				'celcius' => (float)format_number($weather_h_tfl_c, 2, '.'),
				'fahrenheit' => (float)format_number($weather_h_tfl_f, 2, '.'),
				'kelvin' => (float)format_number($weather_h_tfl_k, 2, '.')
			],
			'temperature_dew_point' => [
				'celcius' => (float)format_number($weather_h_tdp_c, 2, '.'),
				'fahrenheit' => (float)format_number($weather_h_tdp_f, 2, '.'),
				'kelvin' => (float)format_number($weather_h_tdp_k, 2, '.')
			],
			'wind' => [
				'descriptions' => [
					'land' => wind_descriptions((float)format_number($weather_h_ws_ms, 1, '.'), 'land'),
					'sea' => wind_descriptions((float)format_number($weather_h_ws_ms, 1, '.'), 'sea')
				],
				'speed' => [
					'ms' => [
						'wind' => (float)format_number($weather_h_ws_ms, 1, '.'),
						'gust' => ($weather_h_ws_g_ms == null ? null : (float)format_number($weather_h_ws_g_ms, 1, '.'))
					],
					'kmh' => [
						'wind' => (float)format_number($weather_h_ws_kmh, 1, '.'),
						'gust' => ($weather_h_ws_g_kmh == null ? null : (float)format_number($weather_h_ws_g_kmh, 1, '.'))
					],
					'mph' => [
						'wind' => (float)format_number($weather_h_ws_mph, 1, '.'),
						'gust' => ($weather_h_ws_g_mph == null ? null : (float)format_number($weather_h_ws_g_mph, 1, '.'))
					],
					'knot' => [
						'wind' => (float)format_number($weather_h_ws_kt, 1, '.'),
						'gust' => ($weather_h_ws_g_kt == null ? null : (float)format_number($weather_h_ws_g_kt, 1, '.'))
					]
				],
				'direction' => [
					'degrees' => (int)$weather_h_wd_deg,
					'compass' => degrees_to_compass((int)$weather_h_wd_deg)
				],
				'beaufort' => beaufort($weather_h_ws_kt)
			],
			'clouds' => (int)$weather_h_c,
			'humidity' => (int)$weather_h_h,
			'h2o_per_kg_of_air' => [
				'value' => format_number((float)$weather_h_h20perkgofair, 2, '.', true),
				'type' => 'g'
			],
			'pressure' => [
				'hpa' => (int)$weather_h_pr,
				'pa' => (int)$weather_h_pr_pa,
				'bar' => (float)format_number($weather_h_pr_bar, 4, '.'),
				'mmhg' => (float)format_number($weather_h_pr_mmhg, 4, '.'),
				'inhg' => (float)format_number($weather_h_pr_inhg, 4, '.'),
				'psi' => (float)format_number($weather_h_pr_psi, 4, '.'),
				'atm' => (float)format_number($weather_h_pr_atm, 4, '.')
			],
			'uv_index' => [
				"grade" => (int)$weather_h_uvi,
				"risk" => uvindex_grade((int)$weather_h_uvi, 'risk'),
				"colour" => uvindex_grade((int)$weather_h_uvi, 'colour')
			],
			'visibility' => [
				'm' => $weather_h_v_m,
				'km' => $weather_h_v_km,
				'mi' => (float)format_number($weather_h_v_mi, 2, '.')
			],
			'astronomy' => [
				'sun' => [
					'altitude' => (float)format_number($astro_sun_altitude, 15, '.'),
					'azimuth' => (float)format_number($astro_sun_azimuth, 15, '.'),
					'is_above_horizon' => $astro_sun_isup,
					'durations' => [
						'daylight' => [
							'hours' => $daylight->h,
							'minutes' => $daylight->i
						],
						'night' => [
							'hours' => $night->h,
							'minutes' => $night->i
						]
					],
					'rise' => [
						'time' => arr_time($astro_sun_rise)
					],
					'set' => [
						'time' => arr_time($astro_sun_set)
					]
				],
				'moon' => [
					'phase' => [
						'phase' => (float)format_number($astro_moon_phase, 15, '.'),
						'name' => $astro_moon_phase_name
					],
					'altitude' => (float)format_number($astro_moon_altitude, 15, '.'),
					'azimuth' => (float)format_number($astro_moon_azimuth, 15, '.'),
					'fraction' => (float)format_number($astro_moon_fraction, 1, '.'),
					'angle' => (float)format_number($astro_moon_angle, 1, '.'),
					'age' => (float)format_number($astro_moon_age, 1, '.'),
					'distances' => [
						'earth' => (float)str_replace(' ', '', format_number($astro_moon_distance_earth, 2, '.')),
						'sun' => (float)str_replace(' ', '', format_number($astro_moon_distance_sun, 2, '.'))
					],
					'diameters' => [
						'moon' => (float)str_replace(' ', '', format_number($astro_moon_diameter_moon, 2, '.')),
						'sun' => (float)str_replace(' ', '', format_number($astro_moon_diameter_sun, 2, '.'))
					],
					'rise' => arr_time($astro_moon_rise),
					'set' => arr_time($astro_moon_set)
				]
			]
		];
	}

?>