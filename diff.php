<?php

	$config = json_decode(file_get_contents(__DIR__.'/config.json'), false);

	require_once 'class-geotimezone.php';
	require_once 'functions.php';
	require_once 'vendor/autoload.php';

	header('Content-Type: application/json;charset=utf-8');



	$arr_aqi = [
		1 => 'Good',
		2 => 'Fair',
		3 => 'Moderate',
		4 => 'Poor',
		5 => 'Very poor'
	];



	$apikey = (!isset($_GET['key']) ? $config->apikey : strip_tags(htmlspecialchars($_GET['key'])));
	$defined_useragent = 'Serenum_serenum.org';

	$default_from_latitude = $config->coordinates->first->latitude;
	$default_from_longitude = $config->coordinates->first->longitude;
	$default_to_latitude = $config->coordinates->second->latitude;
	$default_to_longitude = $config->coordinates->second->longitude;

	$get_coordinates = (!isset($_GET['coor']) ? null : strip_tags(htmlspecialchars($_GET['coor'])));
	$get_language = 'en';

	$filename_path = explode('/', $_SERVER['PHP_SELF']);
	$filename = $filename_path[count($filename_path) - 1];
	$example_path = ($filename == 'index.php' ? null : $filename);

	if(strpos($get_coordinates, ';') !== false) {
		list($coor_from, $coor_to) = explode(';', $get_coordinates);
	}

	$cf = explode(',', $coor_from);
	$ct = explode(',', $coor_to);
	$geotools = new \League\Geotools\Geotools();
	$coor_a = new \League\Geotools\Coordinate\Coordinate([$cf[0], $cf[1]]);
	$coor_b = new \League\Geotools\Coordinate\Coordinate([$ct[0], $ct[1]]);
	$distance = $geotools->distance()->setFrom($coor_a)->setTo($coor_b);



	if($get_coordinates == null) {
		$arr = [
			"id" => 310,
			"message" => "Please enter both latitude and longitude.",
			"example" => "?coor=".$default_from_latitude.",".$default_from_longitude.";".$default_to_latitude.",".$default_to_longitude,
			"example_path" => "/".$example_path."?coor=".$default_from_latitude.",".$default_from_longitude.";".$default_to_latitude.",".$default_to_longitude
		];

	} elseif(strpos($get_coordinates, ';') === false) {
		$arr = [
			"id" => 330,
			"message" => "Please enter from and to coordinates.",
			"example" => "?coor=".$default_from_latitude.",".$default_from_longitude.";".$default_to_latitude.",".$default_to_longitude,
			"example_path" => "/".$example_path."?coor=".$default_from_latitude.",".$default_from_longitude.";".$default_to_latitude.",".$default_to_longitude
		];

	} elseif(strpos($get_coordinates, ';') !== false AND empty($coor_from)) {
		$arr = [
			"id" => 330,
			"message" => "Please enter coordinates for the first place.",
			"example" => "?coor=".$default_from_latitude.",".$default_from_longitude.";".$default_to_latitude.",".$default_to_longitude,
			"example_path" => "/".$example_path."?coor=".$default_from_latitude.",".$default_from_longitude.";".$default_to_latitude.",".$default_to_longitude
		];

	} elseif(strpos($get_coordinates, ';') !== false AND empty($coor_to)) {
		$arr = [
			"id" => 330,
			"message" => "Please enter coordinates for the second place.",
			"example" => "?coor=".$default_from_latitude.",".$default_from_longitude.";".$default_to_latitude.",".$default_to_longitude,
			"example_path" => "/".$example_path."?coor=".$default_from_latitude.",".$default_from_longitude.";".$default_to_latitude.",".$default_to_longitude
		];

	} elseif(!preg_match('/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $coor_from) OR
			 !preg_match('/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $coor_to)) {
		$arr = [
			"id" => 340,
			"message" => "The coordinate are not valid.",
			"example" => "?coor=".$default_from_latitude.",".$default_from_longitude.";".$default_to_latitude.",".$default_to_longitude,
			"example_path" => "/".$example_path."?coor=".$default_from_latitude.",".$default_from_longitude.";".$default_to_latitude.",".$default_to_longitude
		];

	} elseif(!preg_match('/([a-z0-9]{32})$/', $apikey)) {
		$arr = [
			"id" => 350,
			"message" => "The API key are not valid.",
			"example" => MD5('this-is-just-an-example'),
			"example_path" => "/".$example_path."?coor=".$default_from_latitude.",".$default_from_longitude.";".$default_to_latitude.",".$default_to_longitude."&key=".MD5('this-is-just-an-example')
		];




	} else {

		# Split the coordinates
		$coordinates = explode(';', $get_coordinates);



		$count = 0;
		$arr_place = [];

		foreach($coordinates AS $coor) {
			$count++;

			# Get latitude and longitude
			$c = explode(',', $coor);

			$http_options = ['http' => ['method' => 'GET', 'header' => 'User-Agent: '.$defined_useragent]];
			$http_options = stream_context_create($http_options);

			$data_geocoding = json_decode(@file_get_contents('https://nominatim.openstreetmap.org/reverse?lat='.$c[0].'&lon='.$c[1].'&format=json&accept-language='.$get_language, false, $http_options));
			$data_weather = json_decode(@file_get_contents('https://api.openweathermap.org/data/2.5/onecall?lat='.$c[0].'&lon='.$c[1].'&appid='.$apikey.'&units=metric&lang='.$get_language, false, $http_options));
			$data_airpollution = json_decode(@file_get_contents('https://api.openweathermap.org/data/2.5/air_pollution?lat='.$c[0].'&lon='.$c[1].'&appid='.$apikey, false, $http_options));

			$timezone_offset = (!isset($data_weather->timezone_offset) ? null : $data_weather->timezone_offset);

			# Get timezone based on position
			$timezone = $data_weather->timezone;
			$m = new \Moment\Moment('now', $timezone);
			$timestamp = strtotime($m->format('Y-m-d H:i'));

			$geo_historic = (!isset($data_geocoding->address->historic) ? null : $data_geocoding->address->historic);
			$geo_highway = (!isset($data_geocoding->address->highway) ? null : $data_geocoding->address->highway);
			$geo_road = (!isset($data_geocoding->address->road) ? null : $data_geocoding->address->road);
			$geo_neighbourhood = (!isset($data_geocoding->address->neighbourhood) ? null : $data_geocoding->address->neighbourhood);
			$geo_suburb = (!isset($data_geocoding->address->suburb) ? null : $data_geocoding->address->suburb);
			$geo_city = (!isset($data_geocoding->address->city) ? null : $data_geocoding->address->city);
			$geo_municipality = (!isset($data_geocoding->address->municipality) ? null : $data_geocoding->address->municipality);
			$geo_county = (!isset($data_geocoding->address->county) ? null : $data_geocoding->address->county);
			$geo_postcode = (!isset($data_geocoding->address->postcode) ? null : str_replace(' ', '', $data_geocoding->address->postcode));
			$geo_country = (!isset($data_geocoding->address->country) ? null : $data_geocoding->address->country);
			$geo_country_code = (!isset($data_geocoding->address->country_code) ? null : $data_geocoding->address->country_code);
			$geo_full = (!isset($data_geocoding->display_name) ? null : $data_geocoding->display_name);

			$weather_dt_data = (!isset($data_weather->current->dt) ? null : $data_weather->current->dt);
			$weather_id = (!isset($data_weather->current->weather[0]->id) ? null : $data_weather->current->weather[0]->id);
			$weather_ic = (!isset($data_weather->current->weather[0]->icon) ? null : $data_weather->current->weather[0]->icon);
			$weather_wm = (!isset($data_weather->current->weather[0]->main) ? null : $data_weather->current->weather[0]->main);
			$weather_wd = (!isset($data_weather->current->weather[0]->description) ? null : $data_weather->current->weather[0]->description);
			$weather_p = (!isset($data_weather->current->pop) ? null : convert($data_weather->current->pop, 'prop_precip'));
			$weather_r = (!isset($data_weather->current->rain) ? null : $data_weather->current->rain);
			$weather_s = (!isset($data_weather->current->snow) ? null : $data_weather->current->snow);

			$weather_t_c = (!isset($data_weather->current->temp) ? null : $data_weather->current->temp);
			$weather_t_f = ($weather_t_c == null ? null : convert($weather_t_c, 'f'));
			$weather_t_k = ($weather_t_c == null ? null : convert($weather_t_c, 'k'));
			$weather_tfl_c = (!isset($data_weather->current->feels_like) ? null : $data_weather->current->feels_like);
			$weather_tfl_f = ($weather_tfl_c == null ? null : convert($weather_tfl_c, 'f'));
			$weather_tfl_k = ($weather_tfl_c == null ? null : convert($weather_tfl_c, 'k'));
			$weather_tdp_c = (!isset($data_weather->current->dew_point) ? null : $data_weather->current->dew_point);
			$weather_tdp_f = ($weather_tdp_c == null ? null : convert($weather_tdp_c, 'f'));
			$weather_tdp_k = ($weather_tdp_c == null ? null : convert($weather_tdp_c, 'k'));
			$weather_ws_ms = (!isset($data_weather->current->wind_speed) ? null : $data_weather->current->wind_speed);
			$weather_ws_g_ms = (!isset($data_weather->current->wind_gust) ? null : $data_weather->current->wind_gust);
			$weather_ws_kmh = ($weather_ws_ms == null ? null : convert($weather_ws_ms, 'kmh'));
			$weather_ws_g_kmh = ($weather_ws_g_ms == null ? null : convert($weather_ws_g_ms, 'kmh'));
			$weather_ws_mph = ($weather_ws_ms == null ? null : convert($weather_ws_ms, 'mph'));
			$weather_ws_g_mph = ($weather_ws_g_ms == null ? null : convert($weather_ws_g_ms, 'mph'));
			$weather_wd_deg = (!isset($data_weather->current->wind_deg) ? null : $data_weather->current->wind_deg);
			$weather_c = (!isset($data_weather->current->clouds) ? null : $data_weather->current->clouds);
			$weather_uvi = (!isset($data_weather->current->uvi) ? null : $data_weather->current->uvi);
			$weather_pr = (!isset($data_weather->current->pressure) ? null : $data_weather->current->pressure);
			$weather_pr_inhg = (!isset($data_weather->current->pressure) ? null : convert($weather_pr, 'inhg'));
			$weather_h = (!isset($data_weather->current->humidity) ? null : $data_weather->current->humidity);
			$weather_v_m = (!isset($data_weather->current->visibility) ? null : $data_weather->current->visibility);
			$weather_v_km = (!isset($data_weather->current->visibility) ? null : convert($weather_v_m, 'km'));
			$weather_v_mi = (!isset($data_weather->current->visibility) ? null : convert($weather_v_m, 'mi'));

			$airpollution_aqi = (!isset($data_airpollution->list[0]->main->aqi) ? null : $data_airpollution->list[0]->main->aqi);
			$airpollution_co = (!isset($data_airpollution->list[0]->components->co) ? null : $data_airpollution->list[0]->components->co);
			$airpollution_no = (!isset($data_airpollution->list[0]->components->no) ? null : $data_airpollution->list[0]->components->no);
			$airpollution_no2 = (!isset($data_airpollution->list[0]->components->no2) ? null : $data_airpollution->list[0]->components->no2);
			$airpollution_o3 = (!isset($data_airpollution->list[0]->components->o3) ? null : $data_airpollution->list[0]->components->o3);
			$airpollution_so2 = (!isset($data_airpollution->list[0]->components->so2) ? null : $data_airpollution->list[0]->components->so2);
			$airpollution_pm2_5 = (!isset($data_airpollution->list[0]->components->pm2_5) ? null : $data_airpollution->list[0]->components->pm2_5);
			$airpollution_pm10 = (!isset($data_airpollution->list[0]->components->pm10) ? null : $data_airpollution->list[0]->components->pm10);
			$airpollution_nh3 = (!isset($data_airpollution->list[0]->components->nh3) ? null : $data_airpollution->list[0]->components->nh3);







			if(!isset($data_weather)) {
				$arr_place[] = null;


			} else {
				$arr_place[] = [
					"timeanddate" => [
						'timezone' => $timezone,
						'timezone_offset' => $timezone_offset,
						'local_date' => date_($timestamp, 'date'),
						'time' => arr_time($timestamp)
					],
					"place" => [
						'latitude' => (float)$c[0],
						'longitude' => (float)$c[1],
						'historic' => $geo_historic,
						'highway' => $geo_highway,
						'road' => $geo_road,
						'neighbourhood' => $geo_neighbourhood,
						'suburb' => $geo_suburb,
						'city' => $geo_city,
						'municipality' => $geo_municipality,
						'county' => $geo_county,
						'post_code' => $geo_postcode,
						'country' => $geo_country,
						'country_code' => $geo_country_code,
						'full' => $geo_full,
						'link' => 'https://openstreetmap.org/?mlat='.$c[0].'&mlon='.$c[1].'&zoom=15'
					],
					"fetched" => [
						'time' => arr_time($weather_dt_data)
					],
					"weather" => [
						'weather' => [
							'id' => $weather_id,
							'id_icon' => $weather_ic,
							'icon' => weathericon($weather_ic . $weather_id),
							'main' => $weather_wm,
							'description' => $weather_wd,
							'prob_precip' => (float)format_number($weather_p, 0),
							'rain' => [
								'mm' => $weather_r
							],
							'snow' => [
								'mm' => $weather_s
							]
						],
						'temp' => [
							'celcius' => (float)format_number($weather_t_c, 2, '.'),
							'fahrenheit' => (float)format_number($weather_t_f, 2, '.'),
							'kelvin' => (float)format_number($weather_t_k, 2, '.')
						],
						'temp_feels_like' => [
							'celcius' => (float)format_number($weather_tfl_c, 2, '.'),
							'fahrenheit' => (float)format_number($weather_tfl_f, 2, '.'),
							'kelvin' => (float)format_number($weather_tfl_k, 2, '.')
						],
						'temp_dew_point' => [
							'celcius' => (float)format_number($weather_tdp_c, 2, '.'),
							'fahrenheit' => (float)format_number($weather_tdp_f, 2, '.'),
							'kelvin' => (float)format_number($weather_tdp_k, 2, '.')
						],
						'wind' => [
							'descriptions' => [
								'land' => wind_descriptions((float)format_number($weather_ws_ms, 1, '.'), 'land'),
								'sea' => wind_descriptions((float)format_number($weather_ws_ms, 1, '.'), 'sea')
							],
							'speed' => [
								'ms' => [
									'wind' => (float)format_number($weather_ws_ms, 1, '.'),
									'gust' => ($weather_ws_g_ms == null ? null : (float)format_number($weather_ws_g_ms, 1, '.'))
								],
								'kmh' => [
									'wind' => (float)format_number($weather_ws_kmh, 1, '.'),
									'gust' => ($weather_ws_g_kmh == null ? null : (float)format_number($weather_ws_g_kmh, 1, '.'))
								],
								'mph' => [
									'wind' => (float)format_number($weather_ws_mph, 1, '.'),
									'gust' => ($weather_ws_g_mph == null ? null : (float)format_number($weather_ws_g_mph, 1, '.'))
								]
							],
							'direction' => [
								'degrees' => (int)$weather_wd_deg,
								'compass' => degrees_to_compass((int)$weather_wd_deg)
							]
						],
						'clouds' => (int)$weather_c,
						'humidity' => (int)$weather_h,
						'pressure' => [
							'hpa' => (int)$weather_pr,
							'inhg' => (float)format_number($weather_pr_inhg, 4, '.')
						],
						'uv_index' => (int)$weather_uvi,
						'visibility' => [
							'm' => (int)$weather_v_m,
							'km' => (float)format_number($weather_v_km, 2, '.'),
							'mi' => (float)format_number($weather_v_mi, 2, '.')
						],
						'air_pollution' => [
							'quality' => [
								"grade" => air_pollution_quality((int)$airpollution_aqi, 'grade'),
								"colour" => air_pollution_quality((int)$airpollution_aqi, 'colour')
							],
							'aqi' => (int)$airpollution_aqi,
							'co' => (float)$airpollution_co,
							'no' => (float)$airpollution_no,
							'no2' => (float)$airpollution_no2,
							'o3' => (float)$airpollution_o3,
							'so2' => (float)$airpollution_so2,
							'pm25' => (float)$airpollution_pm2_5,
							'pm10' => (float)$airpollution_pm10,
							'nh3' => (float)$airpollution_nh3
						]
					]
				];
			}

		}














		/*
			WORK WITH THE ARRAYS
		*/

		if(empty($arr_place)) {
			$arr = [
				"id" => 200,
				"message" => "Couldn't get any data from at least OpenWeatherMap. This issue can occur when a API key is wrong.",
				"possible_solution" => "Check if the API keys are correct and try again."
			];


		} else {
			$arr = [
				'apikey' => (!isset($_GET['key']) ? 'API key provided by Serenum.' : $apikey),
				'places' => $arr_place,
				"differences" => [
					"distance" => [
						"m" => (int)$distance->greatCircle(),
						"ft" => (float)number_format($distance->in('ft')->greatCircle(), 2, '.', ''),
						"km" => (float)number_format($distance->in('km')->greatCircle(), 2, '.', ''),
						"mi" => (float)number_format($distance->in('mi')->greatCircle(), 2, '.', '')
					],
					"weather" => [
						'temp' => [
							'celcius' => (float)format_number(abs_diff($arr_place[0]['weather']['temp']['celcius'], $arr_place[1]['weather']['temp']['celcius']), 2, '.'),
							'fahrenheit' => (float)format_number(abs_diff($arr_place[0]['weather']['temp']['fahrenheit'], $arr_place[1]['weather']['temp']['fahrenheit']), 2, '.'),
							'kelvin' => (float)format_number(abs_diff($arr_place[0]['weather']['temp']['kelvin'], $arr_place[1]['weather']['temp']['kelvin']), 2, '.')
						],
						'temp_feels_like' => [
							'celcius' => (float)format_number(abs_diff($arr_place[0]['weather']['temp_feels_like']['celcius'], $arr_place[1]['weather']['temp_feels_like']['celcius']), 2, '.'),
							'fahrenheit' => (float)format_number(abs_diff($arr_place[0]['weather']['temp_feels_like']['fahrenheit'], $arr_place[1]['weather']['temp_feels_like']['fahrenheit']), 2, '.'),
							'kelvin' => (float)format_number(abs_diff($arr_place[0]['weather']['temp_feels_like']['kelvin'], $arr_place[1]['weather']['temp_feels_like']['kelvin']), 2, '.')
						],
						'temp_dew_point' => [
							'celcius' => (float)format_number(abs_diff($arr_place[0]['weather']['temp_dew_point']['celcius'], $arr_place[1]['weather']['temp_dew_point']['celcius']), 2, '.'),
							'fahrenheit' => (float)format_number(abs_diff($arr_place[0]['weather']['temp_dew_point']['fahrenheit'], $arr_place[1]['weather']['temp_dew_point']['fahrenheit']), 2, '.'),
							'kelvin' => (float)format_number(abs_diff($arr_place[0]['weather']['temp_dew_point']['kelvin'], $arr_place[1]['weather']['temp_dew_point']['kelvin']), 2, '.')
						],
						'wind' => [
							'speed' => [
								'ms' => [
									'wind' => (float)format_number(abs_diff($arr_place[0]['weather']['wind']['speed']['ms']['wind'], $arr_place[1]['weather']['wind']['speed']['ms']['wind']), 1, '.'),
									'gust' => ($weather_ws_g_ms == null ? null : (float)format_number(abs_diff($arr_place[0]['weather']['wind']['speed']['ms']['gust'], $arr_place[1]['weather']['wind']['speed']['ms']['gust']), 1, '.'))
								],
								'kmh' => [
									'wind' => (float)format_number(abs_diff($arr_place[0]['weather']['wind']['speed']['kmh']['wind'], $arr_place[1]['weather']['wind']['speed']['kmh']['wind']), 1, '.'),
									'gust' => ($weather_ws_g_kmh == null ? null : (float)format_number(abs_diff($arr_place[0]['weather']['wind']['speed']['kmh']['gust'], $arr_place[1]['weather']['wind']['speed']['kmh']['gust']), 1, '.'))
								],
								'mph' => [
									'wind' => (float)format_number(abs_diff($arr_place[0]['weather']['wind']['speed']['mph']['wind'], $arr_place[1]['weather']['wind']['speed']['mph']['wind']), 1, '.'),
									'gust' => ($weather_ws_g_mph == null ? null : (float)format_number(abs_diff($arr_place[0]['weather']['wind']['speed']['mph']['gust'], $arr_place[1]['weather']['wind']['speed']['mph']['gust']), 1, '.'))
								]
							]
						],
						'clouds' => (int)abs_diff($arr_place[0]['weather']['clouds'], $arr_place[1]['weather']['clouds']),
						'humidity' => (int)abs_diff($arr_place[0]['weather']['humidity'], $arr_place[1]['weather']['humidity']),
						'pressure' => [
							'hpa' => (int)abs_diff($arr_place[0]['weather']['pressure']['hpa'], $arr_place[1]['weather']['pressure']['hpa']),
							'inhg' => (float)format_number(abs_diff($arr_place[0]['weather']['pressure']['inhg'], $arr_place[1]['weather']['pressure']['inhg']), 4, '.')
						],
						'uv_index' => (int)abs_diff($arr_place[0]['weather']['uv_index'], $arr_place[1]['weather']['uv_index']),
						'visibility' => [
							'm' => (int)abs_diff($arr_place[0]['weather']['visibility']['m'], $arr_place[1]['weather']['visibility']['m']),
							'km' => (float)format_number(abs_diff($arr_place[0]['weather']['visibility']['km'], $arr_place[1]['weather']['visibility']['km']), 2, '.'),
							'mi' => (float)format_number(abs_diff($arr_place[0]['weather']['visibility']['mi'], $arr_place[1]['weather']['visibility']['mi']), 2, '.')
						]
					]
				]
			];
		}

	}


	echo json_encode($arr);

?>
