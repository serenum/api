<?php

	$config = json_decode(file_get_contents(__DIR__.'/config.json'), false);

	require_once 'class-moonphase.php';
	require_once 'class-suncalc.php';
	require_once 'class-geotimezone.php';
	require_once 'functions.php';

	header('Content-Type: application/json;charset=utf-8');



	$arr_aqi = [
		1 => 'Good',
		2 => 'Fair',
		3 => 'Moderate',
		4 => 'Poor',
		5 => 'Very poor'
	];



	$apikey = (!isset($_GET['key']) ? $config->apikey : strip_tags(htmlspecialchars($_GET['key'])));
	$defined_useragent = 'Serenum_serenum.org';
	$enable_pollen = false;    // This feature has not been added yet

	$default_latitude = $config->coordinates->first->latitude;
	$default_longitude = $config->coordinates->first->longitude;

	$get_coordinates = (!isset($_GET['coor']) ? $default_latitude.','.$default_longitude : strip_tags(htmlspecialchars($_GET['coor'])));
	$get_language = 'en';
	$get_exclude = (!isset($_GET['exclude']) ? null : strip_tags(htmlspecialchars($_GET['exclude'])));
	$get_diff_c1 = (!isset($_GET['c1']) ? null : strip_tags(htmlspecialchars($_GET['c1'])));
	$get_diff_c2 = (!isset($_GET['c2']) ? null : strip_tags(htmlspecialchars($_GET['c2'])));

	$arr_excluded = explode(',', $get_exclude);

	$filename_path = explode('/', $_SERVER['PHP_SELF']);
	$filename = $filename_path[count($filename_path) - 1];
	$example_path = ($filename == 'index.php' ? null : $filename);



	if($get_coordinates == null) {
		$arr = [
			"id" => 110,
			"message" => "Geographical coordinates are missing.",
			"example" => "?coor=".$default_latitude.",".$default_longitude,
			"example_path" => "/".$example_path."?coor=".$default_latitude.",".$default_longitude
		];

	} elseif(!preg_match('/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $get_coordinates)) {
		$arr = [
			"id" => 140,
			"message" => "Geographical coordinates not valid.",
			"example" => "?coor=".$default_latitude.",".$default_longitude,
			"example_path" => "/".$example_path."?coor=".$default_latitude.",".$default_longitude
		];

	} elseif(empty($apikey) OR $apikey == '[your-api-key]') {
		$arr = [
			"id" => 150,
			"message" => "API key from OpenWeatherMap are missing.",
			"example" => MD5('this-is-just-an-example'),
			"example_path" => "/".$example_path."?coor=".$default_latitude.",".$default_longitude."&key=".MD5('this-is-just-an-example')
		];

	} elseif(!preg_match('/([a-z0-9]{32})$/', $apikey)) {
		$arr = [
			"id" => 160,
			"message" => "The API key are not valid.",
			"example" => MD5('this-is-just-an-example'),
			"example_path" => "/".$example_path."?coor=".$default_latitude.",".$default_longitude."&key=".MD5('this-is-just-an-example')
		];


	} elseif($get_exclude != null AND
			 !in_array('place', $arr_excluded) AND
			 !in_array('airp', $arr_excluded) AND
			 !in_array('astronomy', $arr_excluded)) {

		$arr = [
			"id" => 170,
			"message" => "Variables are missing.",
			"example" => "&exclude=airp",
			"example_path" => "/".$example_path."?coor=".$default_latitude.",".$default_longitude."&exclude=airp"
		];




	} else {

		# Extract latitude and longitude from the coordinates
		list($latitude, $longitude) = explode(',', $get_coordinates);

		# Set HTTP options
		$http_options = ['http' => ['method' => 'GET', 'header' => 'User-Agent: '.$defined_useragent]];
		$http_options = stream_context_create($http_options);

		# Get weather data
		$data_geocoding = (in_array('place', $arr_excluded) ? null : json_decode(@file_get_contents('https://nominatim.openstreetmap.org/reverse?lat='.$latitude.'&lon='.$longitude.'&format=json&accept-language='.$get_language, false, $http_options)));
		$data_weather = json_decode(@file_get_contents('https://api.openweathermap.org/data/2.5/onecall?lat='.$latitude.'&lon='.$longitude.'&appid='.$apikey.'&units=metric&lang='.$get_language, false, $http_options));
		$data_airpollution = (in_array('airp', $arr_excluded) ? null : json_decode(@file_get_contents('https://api.openweathermap.org/data/2.5/air_pollution?lat='.$latitude.'&lon='.$longitude.'&appid='.$apikey, false, $http_options)));

		# Get timezone based on position
		$timezone = $data_weather->timezone;
		$datetime = new DateTime(date('Y-m-d H:i:s'));
		$datetime->setTimeZone(new DateTimeZone($timezone));
		$timestamp = strtotime($datetime->format('Y-m-d H:i:s'));

		# Information about the moon
		$astro_moon = new Solaris\MoonPhase();

		# Information about the Sun and the Moon
		$astro = new AurorasLive\SunCalc($datetime, $latitude, $longitude);
		$astro_sun_times = $astro->getSunTimes();
		$astro_sun_position = $astro->getSunPosition();
		$astro_moon_position = $astro->getMoonPosition($datetime);
		$astro_moon_illumination = $astro->getMoonIllumination($datetime);
		$astro_moon_times = $astro->getMoonTimes($datetime);

		$astro_sun_altitude = ($astro_sun_position->altitude * 180 / M_PI);
		$astro_sun_azimuth = ($astro_sun_position->azimuth * 180 / M_PI);
		$astro_sun_isup = ($astro_sun_altitude < 0 ? false : true);
		$astro_sun_rise = ($astro_sun_times['sunrise'] == null ? null : strtotime($astro_sun_times['sunrise']->format('Y-m-d H:i:s')));
		$astro_sun_riseend = ($astro_sun_times['sunriseEnd'] == null ? null : strtotime($astro_sun_times['sunriseEnd']->format('Y-m-d H:i:s')));
		$astro_sun_goldenhourend = ($astro_sun_times['goldenHourEnd'] == null ? null : strtotime($astro_sun_times['goldenHourEnd']->format('Y-m-d H:i:s')));
		$astro_sun_solarnoon = ($astro_sun_times['solarNoon'] == null ? null : strtotime($astro_sun_times['solarNoon']->format('Y-m-d H:i:s')));
		$astro_sun_goldenhour = ($astro_sun_times['goldenHour'] == null ? null : strtotime($astro_sun_times['goldenHour']->format('Y-m-d H:i:s')));
		$astro_sun_setstart = ($astro_sun_times['sunsetStart'] == null ? null : strtotime($astro_sun_times['sunsetStart']->format('Y-m-d H:i:s')));
		$astro_sun_set = ($astro_sun_times['sunset'] == null ? null : strtotime($astro_sun_times['sunset']->format('Y-m-d H:i:s')));
		$astro_sun_dusk = ($astro_sun_times['dusk'] == null ? null : strtotime($astro_sun_times['dusk']->format('Y-m-d H:i:s')));
		$astro_sun_nauticaldusk = ($astro_sun_times['nauticalDusk'] == null ? null : strtotime($astro_sun_times['nauticalDusk']->format('Y-m-d H:i:s')));
		$astro_sun_night = ($astro_sun_times['night'] == null ? null : strtotime($astro_sun_times['night']->format('Y-m-d H:i:s')));
		$astro_sun_nadir = ($astro_sun_times['nadir'] == null ? null : strtotime($astro_sun_times['nadir']->format('Y-m-d H:i:s')));
		$astro_sun_nightend = ($astro_sun_times['nightEnd'] == null ? null : strtotime($astro_sun_times['nightEnd']->format('Y-m-d H:i:s')));
		$astro_sun_nauticaldawn = ($astro_sun_times['nauticalDawn'] == null ? null : strtotime($astro_sun_times['nauticalDawn']->format('Y-m-d H:i:s')));
		$astro_sun_dawn = ($astro_sun_times['dawn'] == null ? null : strtotime($astro_sun_times['dawn']->format('Y-m-d H:i:s')));

		$astro_moon_altitude = ($astro_moon_position->altitude * 180 / M_PI);
		$astro_moon_azimuth = ($astro_moon_position->azimuth * 180 / M_PI);
		$astro_moon_rise = (!isset($astro_moon_times['moonrise']) ? null : strtotime($astro_moon_times['moonrise']->format('Y-m-d H:i:s')));
		$astro_moon_set = (!isset($astro_moon_times['moonset']) ? null : strtotime($astro_moon_times['moonset']->format('Y-m-d H:i:s')));
		$astro_moon_age = $astro_moon->get('age');
		$astro_moon_phase = $astro_moon_illumination['phase'];
		$astro_moon_phase_name = $astro_moon->phase_name();
		$astro_moon_fraction = $astro_moon_illumination['fraction'];
		$astro_moon_angle = $astro_moon_illumination['angle'];
		$astro_moon_distance_earth = $astro_moon->get('distance');
		$astro_moon_distance_sun = $astro_moon->get('sundistance');
		$astro_moon_diameter_moon = $astro_moon->get('diameter');
		$astro_moon_diameter_sun = $astro_moon->get('sundiameter');
		$astro_moon_alwaysup = (isset($astro_moon_times['alwaysUp']) ? true : false);
		$astro_moon_alwaysdown = (isset($astro_moon_times['alwaysDown']) ? true : false);
		$astro_moon_new = $astro_moon->get_phase('new_moon');
		$astro_moon_nextnew = $astro_moon->get_phase('next_new_moon');
		$astro_moon_full = $astro_moon->get_phase('full_moon');
		$astro_moon_nextfull = $astro_moon->get_phase('next_full_moon');
		$astro_moon_firstquarter = $astro_moon->get_phase('first_quarter');
		$astro_moon_nextfirstquarter = $astro_moon->get_phase('next_first_quarter');
		$astro_moon_lastquarter = $astro_moon->get_phase('last_quarter');
		$astro_moon_nextlastquarter = $astro_moon->get_phase('next_last_quarter');

		$timezone_offset = (!isset($data_weather->timezone_offset) ? null : $data_weather->timezone_offset);

		$geo_historic = (!isset($data_geocoding->address->historic) ? null : $data_geocoding->address->historic);
		$geo_highway = (!isset($data_geocoding->address->highway) ? null : $data_geocoding->address->highway);
		$geo_road = (!isset($data_geocoding->address->road) ? null : $data_geocoding->address->road);
		$geo_neighbourhood = (!isset($data_geocoding->address->neighbourhood) ? null : $data_geocoding->address->neighbourhood);
		$geo_suburb = (!isset($data_geocoding->address->suburb) ? null : $data_geocoding->address->suburb);
		$geo_hamlet = (!isset($data_geocoding->address->hamlet) ? null : $data_geocoding->address->hamlet);
		$geo_town = (!isset($data_geocoding->address->town) ? null : $data_geocoding->address->town);
		$geo_village = (!isset($data_geocoding->address->village) ? null : $data_geocoding->address->village);
		$geo_city = (!isset($data_geocoding->address->city) ? null : $data_geocoding->address->city);
		$geo_state = (!isset($data_geocoding->address->state) ? null : $data_geocoding->address->state);
		$geo_state_district = (!isset($data_geocoding->address->state_district) ? null : $data_geocoding->address->state_district);
		$geo_municipality = (!isset($data_geocoding->address->municipality) ? null : $data_geocoding->address->municipality);
		$geo_county = (!isset($data_geocoding->address->county) ? null : $data_geocoding->address->county);
		$geo_postcode = (!isset($data_geocoding->address->postcode) ? null : str_replace(' ', '', $data_geocoding->address->postcode));
		$geo_country = (!isset($data_geocoding->address->country) ? null : $data_geocoding->address->country);
		$geo_country_code = (!isset($data_geocoding->address->country_code) ? null : $data_geocoding->address->country_code);
		$geo_full = (!isset($data_geocoding->display_name) ? null : $data_geocoding->display_name);

		$weather_dt_data = (!isset($data_weather->current->dt) ? null : $data_weather->current->dt);
		$weather_id = (!isset($data_weather->current->weather[0]->id) ? null : $data_weather->current->weather[0]->id);
		$weather_ic = (!isset($data_weather->current->weather[0]->icon) ? null : $data_weather->current->weather[0]->icon);
		$weather_wm = (!isset($data_weather->current->weather[0]->main) ? null : $data_weather->current->weather[0]->main);
		$weather_wd = (!isset($data_weather->current->weather[0]->description) ? null : $data_weather->current->weather[0]->description);
		$weather_r_mm = (!isset($data_weather->current->rain) ? null : $data_weather->current->rain->{'1h'});
		$weather_r_cm = ($weather_r_mm == null ? null : convert($weather_r_mm, 'cm'));
		$weather_r_in = ($weather_r_mm == null ? null : convert($weather_r_mm, 'in'));
		$weather_s_mm = (!isset($data_weather->current->snow) ? null : $data_weather->current->snow->{'1h'});
		$weather_s_cm = ($weather_s_mm == null ? null : convert($weather_s_mm, 'cm'));
		$weather_s_in = ($weather_s_mm == null ? null : convert($weather_s_mm, 'in'));

		$weather_t_c = (!isset($data_weather->current->temp) ? null : $data_weather->current->temp);
		$weather_t_f = ($weather_t_c == null ? null : convert($weather_t_c, 'f'));
		$weather_t_k = ($weather_t_c == null ? null : convert($weather_t_c, 'k'));
		$weather_tfl_c = (!isset($data_weather->current->feels_like) ? null : $data_weather->current->feels_like);
		$weather_tfl_f = (!isset($data_weather->current->feels_like) ? null : convert($weather_tfl_c, 'f'));
		$weather_tfl_k = ($weather_tfl_c == null ? null : convert($weather_tfl_c, 'k'));
		$weather_tdp_c = (!isset($data_weather->current->dew_point) ? null : $data_weather->current->dew_point);
		$weather_tdp_f = (!isset($data_weather->current->dew_point) ? null : convert($weather_tdp_c, 'f'));
		$weather_tdp_k = ($weather_tdp_c == null ? null : convert($weather_tdp_c, 'k'));
		$weather_ws_ms = (!isset($data_weather->current->wind_speed) ? null : $data_weather->current->wind_speed);
		$weather_ws_g_ms = (!isset($data_weather->current->wind_gust) ? null : $data_weather->current->wind_gust);
		$weather_ws_kmh = ($weather_ws_ms == null ? null : convert($weather_ws_ms, 'kmh'));
		$weather_ws_g_kmh = ($weather_ws_g_ms == null ? null : convert($weather_ws_g_ms, 'kmh'));
		$weather_ws_mph = ($weather_ws_ms == null ? null : convert($weather_ws_ms, 'mph'));
		$weather_ws_g_mph = ($weather_ws_g_ms == null ? null : convert($weather_ws_g_ms, 'mph'));
		$weather_ws_kt = ($weather_ws_ms == null ? null : convert($weather_ws_ms, 'knot'));
		$weather_ws_g_kt = ($weather_ws_g_ms == null ? null : convert($weather_ws_g_ms, 'knot'));
		$weather_wd_deg = (!isset($data_weather->current->wind_deg) ? null : $data_weather->current->wind_deg);
		$weather_c = (!isset($data_weather->current->clouds) ? null : $data_weather->current->clouds);
		$weather_uvi = (!isset($data_weather->current->uvi) ? null : $data_weather->current->uvi);
		$weather_pr = (!isset($data_weather->current->pressure) ? null : $data_weather->current->pressure);
		$weather_pr_pa = (!isset($data_weather->current->pressure) ? null : convert($weather_pr, 'pa'));
		$weather_pr_bar = (!isset($data_weather->current->pressure) ? null : convert($weather_pr, 'bar'));
		$weather_pr_mmhg = (!isset($data_weather->current->pressure) ? null : convert($weather_pr, 'mmhg'));
		$weather_pr_inhg = (!isset($data_weather->current->pressure) ? null : convert($weather_pr, 'inhg'));
		$weather_pr_psi = (!isset($data_weather->current->pressure) ? null : convert($weather_pr, 'psi'));
		$weather_pr_atm = (!isset($data_weather->current->pressure) ? null : convert($weather_pr, 'atm'));
		$weather_h = (!isset($data_weather->current->humidity) ? null : $data_weather->current->humidity);
		$weather_h_h20perkgofair = ($weather_h == null ? null : calc_h2o_perkg_air($weather_h, $weather_t_c));
		$weather_v_m = (!isset($data_weather->current->visibility) ? null : $data_weather->current->visibility);
		$weather_v_km = (!isset($data_weather->current->visibility) ? null : convert($weather_v_m, 'km'));
		$weather_v_mi = (!isset($data_weather->current->visibility) ? null : convert($weather_v_m, 'mi'));

		$airpollution_aqi = (!isset($data_airpollution->list[0]->main->aqi) ? null : $data_airpollution->list[0]->main->aqi);
		$airpollution_co = (!isset($data_airpollution->list[0]->components->co) ? null : $data_airpollution->list[0]->components->co);
		$airpollution_no = (!isset($data_airpollution->list[0]->components->no) ? null : $data_airpollution->list[0]->components->no);
		$airpollution_no2 = (!isset($data_airpollution->list[0]->components->no2) ? null : $data_airpollution->list[0]->components->no2);
		$airpollution_o3 = (!isset($data_airpollution->list[0]->components->o3) ? null : $data_airpollution->list[0]->components->o3);
		$airpollution_so2 = (!isset($data_airpollution->list[0]->components->so2) ? null : $data_airpollution->list[0]->components->so2);
		$airpollution_pm2_5 = (!isset($data_airpollution->list[0]->components->pm2_5) ? null : $data_airpollution->list[0]->components->pm2_5);
		$airpollution_pm10 = (!isset($data_airpollution->list[0]->components->pm10) ? null : $data_airpollution->list[0]->components->pm10);
		$airpollution_nh3 = (!isset($data_airpollution->list[0]->components->nh3) ? null : $data_airpollution->list[0]->components->nh3);







		if(!isset($data_weather)) {
			$arr = [
				"id" => 200,
				"message" => "Can't get data from OpenWeatherMap.",
				"possible_solution" => "Check if the API key are correct and try again."
			];


		} else {

			require_once 'api-place.php';
			require_once 'api-current-weather.php';
			require_once 'api-forecast-minutely.php';
			require_once 'api-forecast-hourly.php';
			require_once 'api-forecast-daily.php';
			require_once 'api-alerts.php';
			require_once 'api-airpollution.php';
			require_once 'api-astronomy.php';
			require_once 'api-pollen.php';



			$arr_weather = [
				'fetched' => $arr_wea_fetched,
				'weather' => $arr_wea_current,
				'air_pollution' => (in_array('airp', $arr_excluded) ? null : $arr_airpollution)
			];

			$arr = [
				'apikey' => (!isset($_GET['key']) ? 'API key provided by Serenum.' : $apikey),
				'latitude' => (float)$latitude,
				'longitude' => (float)$longitude,
				'timezone' => [
					'timezone' => $timezone,
					'offset' => $timezone_offset,
					'gmt' => $datetime->format('O'),
					'abbreviation' => $datetime->format('T')
				],
				'datetime' => [
					'local_timestamp' => $timestamp,
					'local_date' => arr_date($timestamp),
					'local_time' => arr_time($timestamp),
					'details' => [
						'year' => (int)date('Y', $timestamp),
						'month' => [
							'number' => (int)date('n', $timestamp),
							'name' => [
								'full' => date('F', $timestamp),
								'short' => date('M', $timestamp)
							]
						],
						'week' => (int)date('W', $timestamp),
						'day' => [
							'number' => (int)date('j', $timestamp),
							'suffix' => date('S', $timestamp),
							'day_of_year' => (int)date('z', $weather_dt_data),
							'weekday' => [
								'full' => date('l', $timestamp),
								'short' => date('D', $timestamp)
							]
						]
					]
				]
			];

			if(!in_array('place', $arr_excluded)) {
				$arr += [
					'place' => $arr_place
				];
			}

			$arr += [
				'weather' => [
					'current' => $arr_weather,
					'minutely' => $arr_wea_minutely,
					'hourly' => $arr_wea_hourly,
					'daily' => $arr_wea_daily,
					'alerts' => (empty($arr_alerts) ? null : $arr_alerts)
				]
			];

			if(!in_array('astronomy', $arr_excluded)) {
				$arr += [
					'astronomy' => $arr_astromoo
				];
			}
		}

	}


	echo json_encode($arr);

?>
