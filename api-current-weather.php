<?php

	$arr_wea_fetched = [
		'time' => arr_time($weather_dt_data)
	];

	$arr_wea_current = [
		'weather' => [
			'id' => $weather_id,
			'id_icon' => $weather_ic,
			'icon' => weathericon($weather_ic . $weather_id),
			'main' => $weather_wm,
			'description' => $weather_wd,
			'rain' => [
				'mm' => (float)format_number($weather_r_mm, 2, '.'),
				'cm' => (float)format_number($weather_r_cm, 2, '.'),
				'in' => (float)format_number($weather_r_in, 2, '.')
			],
			'snow' => [
				'mm' => (float)format_number($weather_s_mm, 2, '.'),
				'cm' => (float)format_number($weather_s_cm, 2, '.'),
				'in' => (float)format_number($weather_s_in, 2, '.')
			],
			'globe_map' => 'https://api.met.no/weatherapi/geosatellite/1.4/?area=global&type=infrared'
		],
		'temp' => [
			'celcius' => (float)format_number($weather_t_c, 2, '.'),
			'fahrenheit' => (float)format_number($weather_t_f, 2, '.'),
			'kelvin' => (float)format_number($weather_t_k, 2, '.')
		],
		'temp_feels_like' => [
			'celcius' => (float)format_number($weather_tfl_c, 2, '.'),
			'fahrenheit' => (float)format_number($weather_tfl_f, 2, '.'),
			'kelvin' => (float)format_number($weather_tfl_k, 2, '.')
		],
		'temp_dew_point' => [
			'celcius' => (float)format_number($weather_tdp_c, 2, '.'),
			'fahrenheit' => (float)format_number($weather_tdp_f, 2, '.'),
			'kelvin' => (float)format_number($weather_tdp_k, 2, '.')
		],
		'wind' => [
			'descriptions' => [
				'land' => wind_descriptions((float)format_number($weather_ws_ms, 1, '.'), 'land'),
				'sea' => wind_descriptions((float)format_number($weather_ws_ms, 1, '.'), 'sea')
			],
			'speed' => [
				'ms' => [
					'wind' => (float)format_number($weather_ws_ms, 1, '.'),
					'gust' => ($weather_ws_g_ms == null ? null : (float)format_number($weather_ws_g_ms, 1, '.'))
				],
				'kmh' => [
					'wind' => (float)format_number($weather_ws_kmh, 1, '.'),
					'gust' => ($weather_ws_g_kmh == null ? null : (float)format_number($weather_ws_g_kmh, 1, '.'))
				],
				'mph' => [
					'wind' => (float)format_number($weather_ws_mph, 1, '.'),
					'gust' => ($weather_ws_g_mph == null ? null : (float)format_number($weather_ws_g_mph, 1, '.'))
				],
				'knot' => [
					'wind' => (float)format_number($weather_ws_kt, 1, '.'),
					'gust' => ($weather_ws_g_kt == null ? null : (float)format_number($weather_ws_g_kt, 1, '.'))
				]
			],
			'direction' => [
				'degrees' => (int)$weather_wd_deg,
				'compass' => degrees_to_compass((int)$weather_wd_deg)
			],
			'beaufort' => beaufort($weather_ws_kt)
		],
		'clouds' => (int)$weather_c,
		'humidity' => (int)$weather_h,
		'h2o_per_kg_of_air' => [
			'value' => format_number((float)$weather_h_h20perkgofair, 2, '.', true),
			'type' => 'g'
		],
		'pressure' => [
			'hpa' => (int)$weather_pr,
			'pa' => (int)$weather_pr_pa,
			'bar' => (float)format_number($weather_pr_bar, 4, '.'),
			'mmhg' => (float)format_number($weather_pr_mmhg, 4, '.'),
			'inhg' => (float)format_number($weather_pr_inhg, 4, '.'),
			'psi' => (float)format_number($weather_pr_psi, 4, '.'),
			'atm' => (float)format_number($weather_pr_atm, 4, '.')
		],
		'uv_index' => [
			"grade" => (int)$weather_uvi,
			"risk" => uvindex_grade((int)$weather_uvi, 'risk'),
			"colour" => uvindex_grade((int)$weather_uvi, 'colour')
		],
		'visibility' => [
			'm' => (int)$weather_v_m,
			'km' => (float)format_number($weather_v_km, 2, '.'),
			'mi' => (float)format_number($weather_v_mi, 2, '.')
		]
	];

?>
